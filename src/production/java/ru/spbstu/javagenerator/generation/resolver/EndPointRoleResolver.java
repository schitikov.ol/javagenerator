package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.model.ucmcomponent.node.EndPoint;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

public class EndPointRoleResolver implements NodeRoleResolver {
    @Override
    public NodeRole resolve(Node node) {
        return NodeRole.PATH_END;
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return clazz == EndPoint.class;
    }
}
