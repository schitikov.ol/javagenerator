package ru.spbstu.javagenerator.model.ucmcomponent;

import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpecComponent;

import java.util.Objects;

public abstract class UcmComponent extends UcmSpecComponent {
    private String name;
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UcmComponent{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UcmComponent that = (UcmComponent) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
