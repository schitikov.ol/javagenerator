package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.StartPoint;

public class StartPointRoleResolver implements NodeRoleResolver {

    @Override
    public NodeRole resolve(Node node) {
        return NodeRole.PATH_START;
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return clazz == StartPoint.class;
    }
}
