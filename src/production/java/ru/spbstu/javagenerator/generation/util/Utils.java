package ru.spbstu.javagenerator.generation.util;

import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.generation.resolver.NodeRole;
import ru.spbstu.javagenerator.generation.resolver.NodeRoleResolverManager;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

public final class Utils {
    public static Node skipIgnoredNodes(Node node) {
        NodeRoleResolverManager resolverManager = new NodeRoleResolverManager();

        while (resolverManager.resolve(node) == NodeRole.IGNORE) {
            node = UcmContextHolder.getUcmContext().getNodeById(node.getSuccNodesIds().get(0));
        }

        return node;
    }
}
