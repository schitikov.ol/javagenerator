package ru.spbstu.javagenerator.generation;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.model.ucmcomponent.UcmComponent;

public final class NodeUtils {
    public static String abbreviateRespDefName(String respDefName) {
        return respDefName.replaceAll("[^a-zA-Z\\d_$]", "_")
                .replaceAll("^\\d", "_");
    }

    public static void addNodeNameComment(CodeBlock.Builder builder, UcmComponent node) {
        builder.add("//$L\n", node.getName());
    }
}
