package ru.spbstu.javagenerator.generation.translator;

public enum TranslationState {
    SIMPLE_PATH, LOOP, BRANCHING
}
