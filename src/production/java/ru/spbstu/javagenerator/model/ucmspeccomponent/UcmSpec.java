package ru.spbstu.javagenerator.model.ucmspeccomponent;

public class UcmSpec extends UcmSpecComponent {
    private UcmDef ucmDef;

    public UcmDef getUcmDef() {
        return ucmDef;
    }

    public void setUcmDef(UcmDef ucmDef) {
        this.ucmDef = ucmDef;
    }
}
