package ru.spbstu.javagenerator.parser.metadata;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.parser.converter.UCMMetadataConverter;

import static ru.spbstu.javagenerator.parser.metadata.UCMMetadataParser.ExprContext;
import static ru.spbstu.javagenerator.parser.metadata.UCMMetadataParser.SpecificationContext;


public final class UCMMetadataConditionTranslator {

    private UCMMetadataConditionTranslator() {
    }

    public static CodeBlock translateCondition(String metadata) {
        UCMMetadataParser parser = UCMMetadataParserFactory.getParserFor(metadata);

        SpecificationContext specificationContext = parser.specification();

        if (parser.getNumberOfSyntaxErrors() > 0) {
            throw new IllegalStateException("Syntax errors was found while parsing condition.");
        }

        return convertToJavaExpression(specificationContext);
    }

    private static CodeBlock convertToJavaExpression(SpecificationContext specificationContext) {
        ExprContext rootExpr;

        try {
            rootExpr = specificationContext.entity().behavior().stimulus().preCondition().condition().expr();
        } catch (NullPointerException ex) {
            return CodeBlock.builder().build();
        }

        return UCMMetadataConverter.convertExpr(rootExpr);
    }

}
