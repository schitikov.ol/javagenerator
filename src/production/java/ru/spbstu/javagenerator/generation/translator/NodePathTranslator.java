package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeSpec;
import ru.spbstu.javagenerator.generation.NodeUtils;
import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.generation.resolver.NodeRole;
import ru.spbstu.javagenerator.generation.resolver.NodeRoleResolverManager;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.StartPoint;

import java.util.HashMap;
import java.util.Map;

import static ru.spbstu.javagenerator.generation.resolver.NodeRole.*;
import static ru.spbstu.javagenerator.generation.translator.TranslationState.*;

public class NodePathTranslator implements RoleTranslator {

    private final StartPoint startPoint;
    private final TypeSpec.Builder environmentClassBuilder;
    private final NodeRoleResolverManager roleResolverManager;
    private final Map<NodeRole, RoleTranslator> nodeRoleToRoleTranslatorMap = new HashMap<>();

    public NodePathTranslator(StartPoint startPoint, TypeSpec.Builder environmentClassBuilder) {
        this.startPoint = startPoint;
        this.environmentClassBuilder = environmentClassBuilder;
        roleResolverManager = new NodeRoleResolverManager();

        nodeRoleToRoleTranslatorMap.put(METHOD, new MethodRoleTranslator());
        nodeRoleToRoleTranslatorMap.put(IF_ELSE_START, new IfElseStartRoleTranslator());
        nodeRoleToRoleTranslatorMap.put(LOOP_START, new LoopStartRoleTranslator());
        nodeRoleToRoleTranslatorMap.put(INNER_UCM, new InnerUcmRoleTranslator());
    }

    private RoleTranslator getNodeRoleTranslatorFor(NodeRole nodeRole) {
        return nodeRoleToRoleTranslatorMap.get(nodeRole);
    }

    public CodeBlock translatePath() {
        return translate(new TranslationContext(this, roleResolverManager, environmentClassBuilder), startPoint).codeBlock;
    }

    @Override
    public TranslationResult translate(TranslationContext translationContext, Node node) {
        CodeBlock.Builder blockBuilder = CodeBlock.builder();
        Node currentNode = node;
        while (true) {
            NodeRole currentNodeRole = roleResolverManager.resolve(currentNode);

            if (currentNodeRole == UNRESOLVED) {
                throw new IllegalStateException("Can't translatePath node with name: \"" + currentNode.getName() +
                        "\" and id: " + currentNode
                        .getId());
            }

            if (needToBreakBefore(translationContext.state, currentNodeRole)) {
                if (currentNodeRole == PATH_END) {
                    NodeUtils.addNodeNameComment(blockBuilder, currentNode);
                }
                break;
            }

            if (currentNodeRole == IGNORE ||
                    currentNodeRole == PATH_START ||
                    currentNodeRole == CONDITION
            ) {
                currentNode = UcmContextHolder.getUcmContext().getNodeById(currentNode.getSuccNodesIds().get(0));
                continue;
            }

            TranslationState state = translationContext.state;

            switch (currentNodeRole) {
                case IF_ELSE_START:
                    translationContext.state = BRANCHING;
                    break;
                case LOOP_START:
                    translationContext.state = LOOP;
                    break;
                case INNER_UCM:
                    translationContext.state = SIMPLE_PATH;
                    break;
            }

            //Here can start chain of recursive calls to "this"
            TranslationResult result = getNodeRoleTranslatorFor(currentNodeRole).translate(translationContext, currentNode);

            translationContext.state = state;

            blockBuilder.add(result.codeBlock);

            currentNode = result.lastNode;
            currentNodeRole = roleResolverManager.resolve(currentNode);

            if (needToBreakAfter(translationContext.state, currentNodeRole)) {
                if (currentNodeRole == PATH_END) {
                    NodeUtils.addNodeNameComment(blockBuilder, currentNode);
                }
                break;
            }

            currentNode = UcmContextHolder.getUcmContext().getNodeById(currentNode.getSuccNodesIds().get(0));
        }

        return new TranslationResult(blockBuilder.build(), currentNode);
    }

    private boolean needToBreakAfter(TranslationState state, NodeRole role) {
        return role == PATH_END || (role == BRANCHING_END && state == BRANCHING);
    }

    private boolean needToBreakBefore(TranslationState state, NodeRole role) {
        return role == PATH_END || role == BRANCHING_END;
    }
}
