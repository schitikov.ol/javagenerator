package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.DirectionArrow;

public class DirectionArrowFactory extends AbstractNodeFactory<DirectionArrow> {
    @Override
    protected DirectionArrow createConcrete() {
        return new DirectionArrow();
    }
}
