package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Stub;

public class StubFactory extends AbstractNodeFactory<Stub> {

    @Override
    protected void postCreate(Stub stub, Element xmlStub) throws DataConversionException {
        super.postCreate(stub, xmlStub);

        long startPointId = xmlStub.getChild("bindings").getChild("in").getAttribute("startPoint").getLongValue();
        stub.setStartPointId(startPointId);
    }

    @Override
    protected Stub createConcrete() {
        return new Stub();
    }
}
