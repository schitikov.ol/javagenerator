package ru.spbstu.javagenerator.parser.metadata;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public final class UCMMetadataParserFactory {

    private UCMMetadataParserFactory() {
    }

    public static UCMMetadataParser getParserFor(String metadata) {
        return new UCMMetadataParser(new CommonTokenStream(new UCMMetadataLexer(CharStreams.fromString(metadata))));
    }
}
