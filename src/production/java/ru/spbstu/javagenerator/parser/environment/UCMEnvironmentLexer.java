// Generated from C:/Users/Linefight/IdeaProjects/javagenerator/src/production/java/ru/spbstu/javagenerator/parser/grammar\UCMEnvironment.g4 by ANTLR 4.7.2
package ru.spbstu.javagenerator.parser.environment;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class UCMEnvironmentLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, ASSIGN=30, DISJUNCTION=31, 
		CONJUNCTION=32, SEMI=33, COLON=34, COMMA=35, ARRAY=36, LIST=37, QUOTE=38, 
		DOUBLE_QUOTE=39, LPAREN=40, RPAREN=41, PLUS=42, MINUS=43, DOT=44, DIV=45, 
		MOD=46, MUL=47, NOT=48, UNDERLINE=49, BIGGER=50, SMALLER=51, EQU=52, BIGGEREQ=53, 
		SMALLEREQ=54, ARROW=55, NONDET=56, IDENTIFIER=57, POSITIVE_INT=58, POSITIVE_REAL=59, 
		BOOL=60, NCONJ=61, WS=62;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
			"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
			"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24", 
			"T__25", "T__26", "T__27", "T__28", "ASSIGN", "DISJUNCTION", "CONJUNCTION", 
			"SEMI", "COLON", "COMMA", "ARRAY", "LIST", "QUOTE", "DOUBLE_QUOTE", "LPAREN", 
			"RPAREN", "PLUS", "MINUS", "DOT", "DIV", "MOD", "MUL", "NOT", "UNDERLINE", 
			"BIGGER", "SMALLER", "EQU", "BIGGEREQ", "SMALLEREQ", "ARROW", "NONDET", 
			"DIGIT", "ALFA", "IDENTIFIER", "POSITIVE_INT", "POSITIVE_REAL", "BOOL", 
			"NCONJ", "SPACE", "NEWLINE", "TAB", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'environment'", "'types'", "'obj'", "'attributes'", "'agent_types'", 
			"'agents'", "'instances'", "'axioms'", "'Nil'", "'logic_formula'", "'reductions'", 
			"'initial'", "'env'", "'agent_parameters'", "'Forall'", "'Exist'", "'of'", 
			"'arg'", "'state'", "'empty'", "'is_in_list'", "'abs'", "'sizeof'", "'get_from_head'", 
			"'get_from_tail'", "'bool'", "'real'", "'integer'", "'symb'", "':='", 
			"'||'", "'&'", "';'", "':'", "','", "'array'", "'list'", "'''", "'\"'", 
			"'('", "')'", "'+'", "'-'", "'.'", "'/'", "'mod'", "'*'", "'~'", "'_'", 
			"'>'", "'<'", "'='", "'>='", "'<='", "'->'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "ASSIGN", "DISJUNCTION", "CONJUNCTION", 
			"SEMI", "COLON", "COMMA", "ARRAY", "LIST", "QUOTE", "DOUBLE_QUOTE", "LPAREN", 
			"RPAREN", "PLUS", "MINUS", "DOT", "DIV", "MOD", "MUL", "NOT", "UNDERLINE", 
			"BIGGER", "SMALLER", "EQU", "BIGGEREQ", "SMALLEREQ", "ARROW", "NONDET", 
			"IDENTIFIER", "POSITIVE_INT", "POSITIVE_REAL", "BOOL", "NCONJ", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public UCMEnvironmentLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "UCMEnvironment.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2@\u01ef\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3"+
		"\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3"+
		"\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3"+
		"\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3"+
		"\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3"+
		"\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3"+
		"\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\37\3"+
		"\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3%\3%\3%\3%\3&\3&\3"+
		"&\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3/\3/"+
		"\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66"+
		"\3\66\3\67\3\67\3\67\38\38\38\39\39\3:\3:\3;\3;\3<\3<\5<\u01bc\n<\3<\3"+
		"<\3<\7<\u01c1\n<\f<\16<\u01c4\13<\3=\6=\u01c7\n=\r=\16=\u01c8\3>\3>\3"+
		">\3>\5>\u01cf\n>\3?\3?\3?\3?\3?\3?\3?\3?\3?\5?\u01da\n?\3@\3@\3A\3A\3"+
		"B\5B\u01e1\nB\3B\3B\3C\3C\3D\3D\3D\6D\u01ea\nD\rD\16D\u01eb\3D\3D\2\2"+
		"E\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20"+
		"\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37"+
		"= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o"+
		"9q:s\2u\2w;y<{=}>\177?\u0081\2\u0083\2\u0085\2\u0087@\3\2\4\3\2\62;\4"+
		"\2C\\c|\2\u01f4\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3"+
		"\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2"+
		"\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3"+
		"\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2"+
		"\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\2"+
		"9\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3"+
		"\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2"+
		"\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2"+
		"_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3"+
		"\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2"+
		"\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0087\3\2\2\2\3\u0089\3\2\2\2\5\u0095\3"+
		"\2\2\2\7\u009b\3\2\2\2\t\u009f\3\2\2\2\13\u00aa\3\2\2\2\r\u00b6\3\2\2"+
		"\2\17\u00bd\3\2\2\2\21\u00c7\3\2\2\2\23\u00ce\3\2\2\2\25\u00d2\3\2\2\2"+
		"\27\u00e0\3\2\2\2\31\u00eb\3\2\2\2\33\u00f3\3\2\2\2\35\u00f7\3\2\2\2\37"+
		"\u0108\3\2\2\2!\u010f\3\2\2\2#\u0115\3\2\2\2%\u0118\3\2\2\2\'\u011c\3"+
		"\2\2\2)\u0122\3\2\2\2+\u0128\3\2\2\2-\u0133\3\2\2\2/\u0137\3\2\2\2\61"+
		"\u013e\3\2\2\2\63\u014c\3\2\2\2\65\u015a\3\2\2\2\67\u015f\3\2\2\29\u0164"+
		"\3\2\2\2;\u016c\3\2\2\2=\u0171\3\2\2\2?\u0174\3\2\2\2A\u0177\3\2\2\2C"+
		"\u0179\3\2\2\2E\u017b\3\2\2\2G\u017d\3\2\2\2I\u017f\3\2\2\2K\u0185\3\2"+
		"\2\2M\u018a\3\2\2\2O\u018c\3\2\2\2Q\u018e\3\2\2\2S\u0190\3\2\2\2U\u0192"+
		"\3\2\2\2W\u0194\3\2\2\2Y\u0196\3\2\2\2[\u0198\3\2\2\2]\u019a\3\2\2\2_"+
		"\u019e\3\2\2\2a\u01a0\3\2\2\2c\u01a2\3\2\2\2e\u01a4\3\2\2\2g\u01a6\3\2"+
		"\2\2i\u01a8\3\2\2\2k\u01aa\3\2\2\2m\u01ad\3\2\2\2o\u01b0\3\2\2\2q\u01b3"+
		"\3\2\2\2s\u01b5\3\2\2\2u\u01b7\3\2\2\2w\u01bb\3\2\2\2y\u01c6\3\2\2\2{"+
		"\u01ca\3\2\2\2}\u01d9\3\2\2\2\177\u01db\3\2\2\2\u0081\u01dd\3\2\2\2\u0083"+
		"\u01e0\3\2\2\2\u0085\u01e4\3\2\2\2\u0087\u01e9\3\2\2\2\u0089\u008a\7g"+
		"\2\2\u008a\u008b\7p\2\2\u008b\u008c\7x\2\2\u008c\u008d\7k\2\2\u008d\u008e"+
		"\7t\2\2\u008e\u008f\7q\2\2\u008f\u0090\7p\2\2\u0090\u0091\7o\2\2\u0091"+
		"\u0092\7g\2\2\u0092\u0093\7p\2\2\u0093\u0094\7v\2\2\u0094\4\3\2\2\2\u0095"+
		"\u0096\7v\2\2\u0096\u0097\7{\2\2\u0097\u0098\7r\2\2\u0098\u0099\7g\2\2"+
		"\u0099\u009a\7u\2\2\u009a\6\3\2\2\2\u009b\u009c\7q\2\2\u009c\u009d\7d"+
		"\2\2\u009d\u009e\7l\2\2\u009e\b\3\2\2\2\u009f\u00a0\7c\2\2\u00a0\u00a1"+
		"\7v\2\2\u00a1\u00a2\7v\2\2\u00a2\u00a3\7t\2\2\u00a3\u00a4\7k\2\2\u00a4"+
		"\u00a5\7d\2\2\u00a5\u00a6\7w\2\2\u00a6\u00a7\7v\2\2\u00a7\u00a8\7g\2\2"+
		"\u00a8\u00a9\7u\2\2\u00a9\n\3\2\2\2\u00aa\u00ab\7c\2\2\u00ab\u00ac\7i"+
		"\2\2\u00ac\u00ad\7g\2\2\u00ad\u00ae\7p\2\2\u00ae\u00af\7v\2\2\u00af\u00b0"+
		"\7a\2\2\u00b0\u00b1\7v\2\2\u00b1\u00b2\7{\2\2\u00b2\u00b3\7r\2\2\u00b3"+
		"\u00b4\7g\2\2\u00b4\u00b5\7u\2\2\u00b5\f\3\2\2\2\u00b6\u00b7\7c\2\2\u00b7"+
		"\u00b8\7i\2\2\u00b8\u00b9\7g\2\2\u00b9\u00ba\7p\2\2\u00ba\u00bb\7v\2\2"+
		"\u00bb\u00bc\7u\2\2\u00bc\16\3\2\2\2\u00bd\u00be\7k\2\2\u00be\u00bf\7"+
		"p\2\2\u00bf\u00c0\7u\2\2\u00c0\u00c1\7v\2\2\u00c1\u00c2\7c\2\2\u00c2\u00c3"+
		"\7p\2\2\u00c3\u00c4\7e\2\2\u00c4\u00c5\7g\2\2\u00c5\u00c6\7u\2\2\u00c6"+
		"\20\3\2\2\2\u00c7\u00c8\7c\2\2\u00c8\u00c9\7z\2\2\u00c9\u00ca\7k\2\2\u00ca"+
		"\u00cb\7q\2\2\u00cb\u00cc\7o\2\2\u00cc\u00cd\7u\2\2\u00cd\22\3\2\2\2\u00ce"+
		"\u00cf\7P\2\2\u00cf\u00d0\7k\2\2\u00d0\u00d1\7n\2\2\u00d1\24\3\2\2\2\u00d2"+
		"\u00d3\7n\2\2\u00d3\u00d4\7q\2\2\u00d4\u00d5\7i\2\2\u00d5\u00d6\7k\2\2"+
		"\u00d6\u00d7\7e\2\2\u00d7\u00d8\7a\2\2\u00d8\u00d9\7h\2\2\u00d9\u00da"+
		"\7q\2\2\u00da\u00db\7t\2\2\u00db\u00dc\7o\2\2\u00dc\u00dd\7w\2\2\u00dd"+
		"\u00de\7n\2\2\u00de\u00df\7c\2\2\u00df\26\3\2\2\2\u00e0\u00e1\7t\2\2\u00e1"+
		"\u00e2\7g\2\2\u00e2\u00e3\7f\2\2\u00e3\u00e4\7w\2\2\u00e4\u00e5\7e\2\2"+
		"\u00e5\u00e6\7v\2\2\u00e6\u00e7\7k\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9"+
		"\7p\2\2\u00e9\u00ea\7u\2\2\u00ea\30\3\2\2\2\u00eb\u00ec\7k\2\2\u00ec\u00ed"+
		"\7p\2\2\u00ed\u00ee\7k\2\2\u00ee\u00ef\7v\2\2\u00ef\u00f0\7k\2\2\u00f0"+
		"\u00f1\7c\2\2\u00f1\u00f2\7n\2\2\u00f2\32\3\2\2\2\u00f3\u00f4\7g\2\2\u00f4"+
		"\u00f5\7p\2\2\u00f5\u00f6\7x\2\2\u00f6\34\3\2\2\2\u00f7\u00f8\7c\2\2\u00f8"+
		"\u00f9\7i\2\2\u00f9\u00fa\7g\2\2\u00fa\u00fb\7p\2\2\u00fb\u00fc\7v\2\2"+
		"\u00fc\u00fd\7a\2\2\u00fd\u00fe\7r\2\2\u00fe\u00ff\7c\2\2\u00ff\u0100"+
		"\7t\2\2\u0100\u0101\7c\2\2\u0101\u0102\7o\2\2\u0102\u0103\7g\2\2\u0103"+
		"\u0104\7v\2\2\u0104\u0105\7g\2\2\u0105\u0106\7t\2\2\u0106\u0107\7u\2\2"+
		"\u0107\36\3\2\2\2\u0108\u0109\7H\2\2\u0109\u010a\7q\2\2\u010a\u010b\7"+
		"t\2\2\u010b\u010c\7c\2\2\u010c\u010d\7n\2\2\u010d\u010e\7n\2\2\u010e "+
		"\3\2\2\2\u010f\u0110\7G\2\2\u0110\u0111\7z\2\2\u0111\u0112\7k\2\2\u0112"+
		"\u0113\7u\2\2\u0113\u0114\7v\2\2\u0114\"\3\2\2\2\u0115\u0116\7q\2\2\u0116"+
		"\u0117\7h\2\2\u0117$\3\2\2\2\u0118\u0119\7c\2\2\u0119\u011a\7t\2\2\u011a"+
		"\u011b\7i\2\2\u011b&\3\2\2\2\u011c\u011d\7u\2\2\u011d\u011e\7v\2\2\u011e"+
		"\u011f\7c\2\2\u011f\u0120\7v\2\2\u0120\u0121\7g\2\2\u0121(\3\2\2\2\u0122"+
		"\u0123\7g\2\2\u0123\u0124\7o\2\2\u0124\u0125\7r\2\2\u0125\u0126\7v\2\2"+
		"\u0126\u0127\7{\2\2\u0127*\3\2\2\2\u0128\u0129\7k\2\2\u0129\u012a\7u\2"+
		"\2\u012a\u012b\7a\2\2\u012b\u012c\7k\2\2\u012c\u012d\7p\2\2\u012d\u012e"+
		"\7a\2\2\u012e\u012f\7n\2\2\u012f\u0130\7k\2\2\u0130\u0131\7u\2\2\u0131"+
		"\u0132\7v\2\2\u0132,\3\2\2\2\u0133\u0134\7c\2\2\u0134\u0135\7d\2\2\u0135"+
		"\u0136\7u\2\2\u0136.\3\2\2\2\u0137\u0138\7u\2\2\u0138\u0139\7k\2\2\u0139"+
		"\u013a\7|\2\2\u013a\u013b\7g\2\2\u013b\u013c\7q\2\2\u013c\u013d\7h\2\2"+
		"\u013d\60\3\2\2\2\u013e\u013f\7i\2\2\u013f\u0140\7g\2\2\u0140\u0141\7"+
		"v\2\2\u0141\u0142\7a\2\2\u0142\u0143\7h\2\2\u0143\u0144\7t\2\2\u0144\u0145"+
		"\7q\2\2\u0145\u0146\7o\2\2\u0146\u0147\7a\2\2\u0147\u0148\7j\2\2\u0148"+
		"\u0149\7g\2\2\u0149\u014a\7c\2\2\u014a\u014b\7f\2\2\u014b\62\3\2\2\2\u014c"+
		"\u014d\7i\2\2\u014d\u014e\7g\2\2\u014e\u014f\7v\2\2\u014f\u0150\7a\2\2"+
		"\u0150\u0151\7h\2\2\u0151\u0152\7t\2\2\u0152\u0153\7q\2\2\u0153\u0154"+
		"\7o\2\2\u0154\u0155\7a\2\2\u0155\u0156\7v\2\2\u0156\u0157\7c\2\2\u0157"+
		"\u0158\7k\2\2\u0158\u0159\7n\2\2\u0159\64\3\2\2\2\u015a\u015b\7d\2\2\u015b"+
		"\u015c\7q\2\2\u015c\u015d\7q\2\2\u015d\u015e\7n\2\2\u015e\66\3\2\2\2\u015f"+
		"\u0160\7t\2\2\u0160\u0161\7g\2\2\u0161\u0162\7c\2\2\u0162\u0163\7n\2\2"+
		"\u01638\3\2\2\2\u0164\u0165\7k\2\2\u0165\u0166\7p\2\2\u0166\u0167\7v\2"+
		"\2\u0167\u0168\7g\2\2\u0168\u0169\7i\2\2\u0169\u016a\7g\2\2\u016a\u016b"+
		"\7t\2\2\u016b:\3\2\2\2\u016c\u016d\7u\2\2\u016d\u016e\7{\2\2\u016e\u016f"+
		"\7o\2\2\u016f\u0170\7d\2\2\u0170<\3\2\2\2\u0171\u0172\7<\2\2\u0172\u0173"+
		"\7?\2\2\u0173>\3\2\2\2\u0174\u0175\7~\2\2\u0175\u0176\7~\2\2\u0176@\3"+
		"\2\2\2\u0177\u0178\7(\2\2\u0178B\3\2\2\2\u0179\u017a\7=\2\2\u017aD\3\2"+
		"\2\2\u017b\u017c\7<\2\2\u017cF\3\2\2\2\u017d\u017e\7.\2\2\u017eH\3\2\2"+
		"\2\u017f\u0180\7c\2\2\u0180\u0181\7t\2\2\u0181\u0182\7t\2\2\u0182\u0183"+
		"\7c\2\2\u0183\u0184\7{\2\2\u0184J\3\2\2\2\u0185\u0186\7n\2\2\u0186\u0187"+
		"\7k\2\2\u0187\u0188\7u\2\2\u0188\u0189\7v\2\2\u0189L\3\2\2\2\u018a\u018b"+
		"\7)\2\2\u018bN\3\2\2\2\u018c\u018d\7$\2\2\u018dP\3\2\2\2\u018e\u018f\7"+
		"*\2\2\u018fR\3\2\2\2\u0190\u0191\7+\2\2\u0191T\3\2\2\2\u0192\u0193\7-"+
		"\2\2\u0193V\3\2\2\2\u0194\u0195\7/\2\2\u0195X\3\2\2\2\u0196\u0197\7\60"+
		"\2\2\u0197Z\3\2\2\2\u0198\u0199\7\61\2\2\u0199\\\3\2\2\2\u019a\u019b\7"+
		"o\2\2\u019b\u019c\7q\2\2\u019c\u019d\7f\2\2\u019d^\3\2\2\2\u019e\u019f"+
		"\7,\2\2\u019f`\3\2\2\2\u01a0\u01a1\7\u0080\2\2\u01a1b\3\2\2\2\u01a2\u01a3"+
		"\7a\2\2\u01a3d\3\2\2\2\u01a4\u01a5\7@\2\2\u01a5f\3\2\2\2\u01a6\u01a7\7"+
		">\2\2\u01a7h\3\2\2\2\u01a8\u01a9\7?\2\2\u01a9j\3\2\2\2\u01aa\u01ab\7@"+
		"\2\2\u01ab\u01ac\7?\2\2\u01acl\3\2\2\2\u01ad\u01ae\7>\2\2\u01ae\u01af"+
		"\7?\2\2\u01afn\3\2\2\2\u01b0\u01b1\7/\2\2\u01b1\u01b2\7@\2\2\u01b2p\3"+
		"\2\2\2\u01b3\u01b4\5? \2\u01b4r\3\2\2\2\u01b5\u01b6\t\2\2\2\u01b6t\3\2"+
		"\2\2\u01b7\u01b8\t\3\2\2\u01b8v\3\2\2\2\u01b9\u01bc\5u;\2\u01ba\u01bc"+
		"\5c\62\2\u01bb\u01b9\3\2\2\2\u01bb\u01ba\3\2\2\2\u01bc\u01c2\3\2\2\2\u01bd"+
		"\u01c1\5u;\2\u01be\u01c1\5c\62\2\u01bf\u01c1\5s:\2\u01c0\u01bd\3\2\2\2"+
		"\u01c0\u01be\3\2\2\2\u01c0\u01bf\3\2\2\2\u01c1\u01c4\3\2\2\2\u01c2\u01c0"+
		"\3\2\2\2\u01c2\u01c3\3\2\2\2\u01c3x\3\2\2\2\u01c4\u01c2\3\2\2\2\u01c5"+
		"\u01c7\5s:\2\u01c6\u01c5\3\2\2\2\u01c7\u01c8\3\2\2\2\u01c8\u01c6\3\2\2"+
		"\2\u01c8\u01c9\3\2\2\2\u01c9z\3\2\2\2\u01ca\u01ce\5y=\2\u01cb\u01cc\5"+
		"Y-\2\u01cc\u01cd\5y=\2\u01cd\u01cf\3\2\2\2\u01ce\u01cb\3\2\2\2\u01ce\u01cf"+
		"\3\2\2\2\u01cf|\3\2\2\2\u01d0\u01d1\7v\2\2\u01d1\u01d2\7t\2\2\u01d2\u01d3"+
		"\7w\2\2\u01d3\u01da\7g\2\2\u01d4\u01d5\7h\2\2\u01d5\u01d6\7c\2\2\u01d6"+
		"\u01d7\7n\2\2\u01d7\u01d8\7u\2\2\u01d8\u01da\7g\2\2\u01d9\u01d0\3\2\2"+
		"\2\u01d9\u01d4\3\2\2\2\u01da~\3\2\2\2\u01db\u01dc\5C\"\2\u01dc\u0080\3"+
		"\2\2\2\u01dd\u01de\7\"\2\2\u01de\u0082\3\2\2\2\u01df\u01e1\7\17\2\2\u01e0"+
		"\u01df\3\2\2\2\u01e0\u01e1\3\2\2\2\u01e1\u01e2\3\2\2\2\u01e2\u01e3\7\f"+
		"\2\2\u01e3\u0084\3\2\2\2\u01e4\u01e5\7\13\2\2\u01e5\u0086\3\2\2\2\u01e6"+
		"\u01ea\5\u0081A\2\u01e7\u01ea\5\u0083B\2\u01e8\u01ea\5\u0085C\2\u01e9"+
		"\u01e6\3\2\2\2\u01e9\u01e7\3\2\2\2\u01e9\u01e8\3\2\2\2\u01ea\u01eb\3\2"+
		"\2\2\u01eb\u01e9\3\2\2\2\u01eb\u01ec\3\2\2\2\u01ec\u01ed\3\2\2\2\u01ed"+
		"\u01ee\bD\2\2\u01ee\u0088\3\2\2\2\f\2\u01bb\u01c0\u01c2\u01c8\u01ce\u01d9"+
		"\u01e0\u01e9\u01eb\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}