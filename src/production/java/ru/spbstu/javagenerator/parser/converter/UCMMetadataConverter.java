package ru.spbstu.javagenerator.parser.converter;

import com.squareup.javapoet.CodeBlock;
import org.antlr.v4.runtime.RuleContext;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;
import static ru.spbstu.javagenerator.parser.metadata.UCMMetadataParser.*;


public final class UCMMetadataConverter {

    private static final Map<String, String> ucmToJavaOperationsMap = ofEntries(
            entry("*", "*"),
            entry("/", "/"),
            entry("+", "+"),
            entry("-", "-"),
            entry(">", ">"),
            entry("<", "<"),
            entry(">=", ">="),
            entry("<=", "<="),
            entry("==", "=="),
            entry("!=", "!="),
            entry(":=", "="),
            entry("||", "||"),
            entry("&&", "&&"),
            entry("mod", "%"),
            entry("rem", "%"),
            entry("!", "!")
    );

    private UCMMetadataConverter() {
    }

    public static CodeBlock convertSpecification(SpecificationContext specification) {
        EntityContext entity = specification.entity();

        CodeBlock.Builder cb = CodeBlock.builder();

        for (LocalDeclContext localDecl : entity.localDecl()) {
            cb.add(convertLocalDecl(localDecl));
        }

        return cb.add(convertBehavior(entity.behavior())).build();
    }

    public static CodeBlock convertBehavior(BehaviorContext behavior) {
        CodeBlock.Builder cb = CodeBlock.builder();

        if (behavior.stimulus() != null) {
            cb.add(convertStimulus(behavior.stimulus()));
        }

        cb.add(convertAction(behavior.action()));

        if (behavior.stimulus() != null && behavior.stimulus().preCondition() != null) {
            cb.endControlFlow();
        }

        return cb.build();
    }

    public static CodeBlock convertStimulus(StimulusContext stimulus) {
        CodeBlock.Builder cb = CodeBlock.builder();

        if (stimulus.trigger() == null) {
            return cb.beginControlFlow("if ($L)", convertExpr(stimulus.preCondition().condition().expr())).build();
        }

        if (stimulus.preCondition() == null) {
            return cb.add(convertTrigger(stimulus.trigger())).build();
        }

        if (stimulus.preCondition().equals(stimulus.children.get(0))) {
            return cb
                    .beginControlFlow("if ($L)", convertExpr(stimulus.preCondition().condition().expr()))
                    .add(convertTrigger(stimulus.trigger()))
                    .build();
        } else {
            return cb
                    .add(convertTrigger(stimulus.trigger()))
                    .beginControlFlow("if ($L)", convertExpr(stimulus.preCondition().condition().expr()))
                    .build();
        }

    }

    public static CodeBlock convertTrigger(TriggerContext trigger) {
        if (trigger.name() != null) {
            throw new UnsupportedOperationException("'timeout' is not supported yet.");
        }

        return CodeBlock.builder().addStatement(
                "IOManager.receiveSignal($L, $L)",
                convertCall(trigger.envent().call()),
                convertName(trigger.instance().name())
        ).build();
    }

    public static CodeBlock convertLocalDecl(LocalDeclContext localDecl) {
        return CodeBlock.builder()
                .addStatement(
                        "$L $L = $L",
                        convertName(localDecl.type().name()),
                        convertName(localDecl.name()),
                        Optional.ofNullable(localDecl.defaultt())
                                .map(DefaulttContext::expr)
                                .map(UCMMetadataConverter::convertExpr)
                                .orElse(CodeBlock.builder().add("$L", "null").build())
                )
                .build();
    }

    public static CodeBlock convertAction(ActionContext action) {
        if (action.stmt() == null) {
            return CodeBlock.builder().build();
        }

        CodeBlock.Builder cb = CodeBlock.builder();
        for (StmtContext stmt : action.stmt()) {
            cb.add(convertStmt(stmt));
        }

        return cb.build();
    }

    public static CodeBlock convertStmt(StmtContext stmt) {
        if (stmt.assigment() != null) {
            return CodeBlock.builder().addStatement(convertAssigment(stmt.assigment())).build();
        }

        if (stmt.expr() != null) {
            return CodeBlock.builder().addStatement(convertExpr(stmt.expr())).build();
        }

        if (!stmt.output().isEmpty()) {
            CodeBlock.Builder cb = CodeBlock.builder();
            for (OutputContext output : stmt.output()) {
                cb.addStatement(convertOutput(output));
            }

            return cb.build();
        }

        throw new UnsupportedOperationException("'reset', 'set' and 'stop' are not supported yet.");
    }

    public static CodeBlock convertOutput(OutputContext output) {
        return CodeBlock.builder()
                .add(
                        "IOManager.sendSignal($L, $L)",
                        convertCall(output.call()),
                        convertName(output.instance().name())
                )
                .build();
    }

    public static CodeBlock convertAssigment(AssigmentContext assigment) {
        return CodeBlock.builder().add("$L = $L", convertLvalue(assigment.lvalue()), convertExpr(assigment.expr())).build();
    }

    public static CodeBlock convertLvalue(LvalueContext lvalue) {
        if (lvalue.simpleName() != null) {
            return convertSimpleNameActual(lvalue.simpleName(), lvalue.actual());
        }

        if (lvalue.name() != null) {
            return convertName(lvalue.name());
        }

        if (lvalue.ownedAttr() != null) {
            return convertOwnedAttr(lvalue.ownedAttr());
        }

        if (lvalue.lvalue() != null) {
            return convertLvalue(lvalue.lvalue());
        }

        throw new UnsupportedOperationException("'this' and 'signal' are not supported as lvalue.");
    }

    public static CodeBlock convertExpr(ExprContext expr) {
        return convertExpr(
                expr::expr1,
                UCMMetadataConverter::convertExpr1,
                expr::exprOp
        );
    }

    public static CodeBlock convertExpr1(Expr1Context expr1) {
        return convertExpr(
                expr1::expr2,
                UCMMetadataConverter::convertExpr2,
                expr1::expr1Op
        );
    }

    public static CodeBlock convertExpr2(Expr2Context expr2) {
        CodeBlock.Builder cb = CodeBlock.builder();

        if (expr2.expr3().size() == 1) {
            return convertExpr3(expr2.expr3(0));
        }

        CodeBlock currOperand = convertExpr3(expr2.expr3(0));

        for (int i = 1; i < expr2.expr3().size(); i++) {
            CodeBlock nextOperand = convertExpr3(expr2.expr3(i));

            if (expr2.expr2Op(i - 1).NOT_EQU() != null) {
                cb.add("$L", "!");
            }

            cb.add("$T.equals($L, $L)", Objects.class, currOperand, nextOperand);

            currOperand = nextOperand;

            if (i < expr2.expr3().size() - 1) {
                cb.add("$L", " && ");
            }
        }


        return cb.build();
    }

    public static CodeBlock convertExpr3(Expr3Context expr3) {
        return convertExpr(
                expr3::expr4,
                UCMMetadataConverter::convertExpr4,
                expr3::expr3Op
        );
    }

    public static CodeBlock convertExpr4(Expr4Context expr4) {
        return convertExpr(
                expr4::expr5,
                UCMMetadataConverter::convertExpr5,
                expr4::expr4Op
        );
    }

    public static CodeBlock convertExpr5(Expr5Context expr5) {
        return convertExpr(
                expr5::term,
                UCMMetadataConverter::convertTerm,
                expr5::expr5Op
        );
    }

    public static CodeBlock convertTerm(TermContext term) {
        if (term.imperative() != null) {
            throw new UnsupportedOperationException("Imperative is not implemented yet.");
        }

        if (term.termOp() != null && term.term() != null) {
            CodeBlock.Builder cb = CodeBlock.builder();

            return cb.add(convertExprOp(term.termOp().getText())).add(convertTerm(term.term())).build();
        }

        if (term.primary() != null) {
            return convertPrimary(term.primary());
        }

        throw new IllegalArgumentException("Term should be termOp + term or primary.");
    }

    public static CodeBlock convertPrimary(PrimaryContext primary) {
        CallOrTargetContext callOrTarget = primary.callOrTarget();

        if (callOrTarget.ownedAttr() != null) {
            return convertOwnedAttr(callOrTarget.ownedAttr());
        }

        if (callOrTarget.target() != null) {
            return convertTarget(callOrTarget.target());
        }

        if (callOrTarget.call() != null) {
            return convertCall(callOrTarget.call());
        }

        throw new IllegalArgumentException("Primary should be one of: target, call, ownedAttr.");
    }

    public static CodeBlock convertOwnedAttr(OwnedAttrContext ownedAttr) {
        CodeBlock.Builder cb = CodeBlock.builder();
        return cb.add("$L.$L", convertName(ownedAttr.name(0)), convertName(ownedAttr.name(1))).build();
    }

    public static CodeBlock convertCall(CallContext call) {
        if (call.target() != null) {
            return convertTarget(call.target());
        }

        if (call.simpleName() != null) {
            return convertSimpleNameActual(call.simpleName(), call.actual());
        }

        throw new IllegalArgumentException("Call should be one of: target, simpleName + actuals.");
    }

    public static CodeBlock convertSimpleNameActual(SimpleNameContext simpleName, List<ActualContext> actuals) {
        CodeBlock.Builder cb = CodeBlock.builder();

        //possible array syntax
        if ("arg".equals(simpleName.getText())) {
            if (actuals.size() < 2) {
                throw new IllegalArgumentException("arg(...) must have more than 2 arguments.");
            }

            cb.add("$L", actuals.get(0).getText());

            for (int i = 1; i < actuals.size(); i++) {
                cb.add("[$L]", convertActual(actuals.get(i)));
            }

            return cb.build();
        }


        //function call
        cb.add("$L", simpleName.getText());

        if (actuals.isEmpty()) {
            return cb.add("$L", "()").build();
        }

        return cb.add(
                actuals
                        .stream()
                        .map(UCMMetadataConverter::convertActual)
                        .collect(CodeBlock.joining(", ", "(", ")"))
        ).build();
    }

    public static CodeBlock convertActual(ActualContext actual) {
        CodeBlock.Builder cb = CodeBlock.builder();


        if (actual.simpleName() != null && actual.expr() != null) {
            return cb.add("$L = $L", actual.simpleName().getText(), convertExpr(actual.expr())).build();
        }

        if (actual.expr() != null) {
            return convertExpr(actual.expr());
        }

        throw new IllegalArgumentException("Actual should be one of: simpleName := expr, expr.");
    }

    public static CodeBlock convertTarget(TargetContext target) {
        CodeBlock.Builder cb = CodeBlock.builder();

        if (target.bracketExpr() != null) {
            return cb.add("($L)", convertExpr(target.bracketExpr().expr())).build();
        }

        if (target.name() != null) {
            return convertName(target.name());
        }

        if (target.literal() != null) {
            return cb.add("$L", target.literal().getText()).build();
        }

        throw new IllegalArgumentException("Target should be one of: bracketExpr, name, literal.");
    }

    public static CodeBlock convertName(NameContext name) {
        return CodeBlock.builder().add("$L", name.getText().replace("::", ".")).build();
    }

    private static <T extends RuleContext> CodeBlock convertExpr(
            //only produce list, only read from produced list
            Supplier<? extends List<? extends T>> childrenExprSupplier,
            //any converter that can work with T
            Function<? super T, CodeBlock> childExprConverter,
            //only produce list, only read from produced list
            Supplier<? extends List<? extends RuleContext>> exprOpsSupplier
    ) {
        CodeBlock.Builder cb = CodeBlock.builder();
        List<? extends T> childrenExpr = childrenExprSupplier.get();
        List<? extends RuleContext> exprOps = exprOpsSupplier.get();

        for (int i = 0; i < childrenExpr.size(); i++) {
            cb.add("$L", childExprConverter.apply(childrenExpr.get(i)));
            if (i < childrenExpr.size() - 1) {
                cb.add(" $L ", convertExprOp(exprOps.get(i).getText()));
            }
        }

        return cb.build();
    }

    private static CodeBlock convertExprOp(String exprOpSymbol) {
        return CodeBlock.builder().add("$L", ucmToJavaOperationsMap.get(exprOpSymbol)).build();
    }
}
