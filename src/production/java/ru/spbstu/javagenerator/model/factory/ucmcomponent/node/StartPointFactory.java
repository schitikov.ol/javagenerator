package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.StartPoint;

public class StartPointFactory extends AbstractNodeFactory<StartPoint> {
    @Override
    protected StartPoint createConcrete() {
        return new StartPoint();
    }
}
