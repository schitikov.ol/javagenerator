package ru.spbstu.javagenerator.parser.converter;

import org.junit.jupiter.api.Test;
import ru.spbstu.javagenerator.parser.metadata.UCMMetadataSpecificationTranslator;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UCMMetadataConverterTest {

    @Test
    public void convertSpecification() {
        assertEquals("Integer a = 5;\nInteger b = 6;\narray()[5 + 3] = 3;\na + b;\n", UCMMetadataSpecificationTranslator.translateSpecification("a : Integer := 5;\nb : Integer := 6;\ndo {\n    arg(array(), 5 + 3) := 3;\n    a + b;\n}").toString());
    }

}