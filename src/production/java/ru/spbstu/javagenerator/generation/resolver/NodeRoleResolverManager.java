package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

import java.util.HashSet;
import java.util.Set;

public class NodeRoleResolverManager implements NodeRoleResolver {

    private Set<NodeRoleResolver> nodeRoleResolvers = new HashSet<>();

    public NodeRoleResolverManager() {
        nodeRoleResolvers.add(new IgnoreNodeRoleResolver());
        nodeRoleResolvers.add(new ResponsibilityRoleResolver());
        nodeRoleResolvers.add(new OrForkRoleResolver(this));
        nodeRoleResolvers.add(new OrJoinRoleResolver());
        nodeRoleResolvers.add(new StartPointRoleResolver());
        nodeRoleResolvers.add(new EndPointRoleResolver());
        nodeRoleResolvers.add(new StubRoleResolver());
    }

    @Override
    public NodeRole resolve(Node node) {
        NodeRole role = NodeRole.UNRESOLVED;
        for (NodeRoleResolver roleResolver : nodeRoleResolvers) {
            if (roleResolver.supports(node.getClass())) {
                role = roleResolver.resolve(node);
                if (role != NodeRole.UNRESOLVED) {
                    break;
                }
            }
        }
        return role;
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        boolean supports = false;

        for (NodeRoleResolver roleResolver : nodeRoleResolvers) {
            if (roleResolver.supports(clazz)) {
                supports = true;
                break;
            }
        }

        return supports;
    }
}
