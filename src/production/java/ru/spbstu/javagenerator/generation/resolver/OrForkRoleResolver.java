package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.generation.util.Utils;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.OrFork;

import java.util.List;
import java.util.stream.Collectors;

public class OrForkRoleResolver implements NodeRoleResolver {

    private final NodeRoleResolverManager roleResolverManager;

    public OrForkRoleResolver(NodeRoleResolverManager roleResolverManager) {
        this.roleResolverManager = roleResolverManager;
    }

    @Override
    public NodeRole resolve(Node node) {
        List<Long> succNodesIds = node.getSuccNodesIds();
        List<Node> succNodes = succNodesIds
                .stream()
                .map(UcmContextHolder.getUcmContext()::getNodeById)
                .collect(Collectors.toList());

        int conditionsCount = 0;
        for (Node succNode : succNodes) {
            succNode = Utils.skipIgnoredNodes(succNode);

            if (roleResolverManager.resolve(succNode) == NodeRole.CONDITION) {
                conditionsCount++;
            }
        }

        if (succNodes.size() >= 2) {
            if ((succNodes.size() - conditionsCount) == 0 || (succNodes.size() - conditionsCount) == 1) {
                return NodeRole.IF_ELSE_START;
            } else {
                if (conditionsCount == 0) {
                    return NodeRole.BRANCHING_END;
                }
            }
        }

        return NodeRole.UNRESOLVED;
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return clazz == OrFork.class;
    }
}
