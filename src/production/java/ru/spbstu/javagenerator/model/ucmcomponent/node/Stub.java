package ru.spbstu.javagenerator.model.ucmcomponent.node;

public class Stub extends Node {
    private long startPointId;


    public long getStartPointId() {
        return startPointId;
    }

    public void setStartPointId(long startPointId) {
        this.startPointId = startPointId;
    }
}
