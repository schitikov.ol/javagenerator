package ru.spbstu.javagenerator.model.ucmspeccomponent;

import ru.spbstu.javagenerator.model.ucmcomponent.Agent;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;
import ru.spbstu.javagenerator.model.ucmcomponent.UcmMap;

import java.util.List;

public class UcmDef extends UcmSpecComponent {
    private List<ResponsibilityDef> responsibilityDefs;
    private List<UcmMap> ucmMaps;
    private List<Agent> agents;

    public List<ResponsibilityDef> getResponsibilityDefs() {
        return responsibilityDefs;
    }

    public void setResponsibilityDefs(List<ResponsibilityDef> responsibilityDefs) {
        this.responsibilityDefs = responsibilityDefs;
    }

    public List<UcmMap> getUcmMaps() {
        return ucmMaps;
    }

    public void setUcmMaps(List<UcmMap> ucmMaps) {
        this.ucmMaps = ucmMaps;
    }

    public List<Agent> getAgents() {
        return agents;
    }

    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }
}
