package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Stub;

import javax.lang.model.element.Modifier;

import static ru.spbstu.javagenerator.generation.NodeUtils.abbreviateRespDefName;

public class InnerUcmRoleTranslator implements RoleTranslator {

    @Override
    public TranslationResult translate(TranslationContext translationContext, Node node) {

        String methodName = abbreviateRespDefName(node.getName());
        MethodSpec.Builder methodSpecBuilder = MethodSpec.methodBuilder(methodName)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(TypeName.VOID);

        TranslationResult translationResult = translationContext.nodePathTranslator.translate(
                translationContext,
                UcmContextHolder.getUcmContext().getNodeById(((Stub) node).getStartPointId())
        );
        methodSpecBuilder.addCode(translationResult.codeBlock);

        translationContext.environmentClassBuilder.addMethod(methodSpecBuilder.build());

        CodeBlock result = CodeBlock.builder().addStatement("$N()", methodName).build();
        return new TranslationResult(result, node);
    }
}
