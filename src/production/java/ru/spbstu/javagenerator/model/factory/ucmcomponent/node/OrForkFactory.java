package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.OrFork;

public class OrForkFactory extends AbstractNodeFactory<OrFork> {
    @Override
    protected OrFork createConcrete() {
        return new OrFork();
    }
}
