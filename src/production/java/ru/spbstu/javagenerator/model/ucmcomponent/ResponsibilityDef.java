package ru.spbstu.javagenerator.model.ucmcomponent;

public class ResponsibilityDef extends UcmComponent {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
