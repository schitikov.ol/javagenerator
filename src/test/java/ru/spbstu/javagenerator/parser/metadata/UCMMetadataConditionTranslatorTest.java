package ru.spbstu.javagenerator.parser.metadata;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;
import static org.junit.jupiter.api.Assertions.*;

class UCMMetadataConditionTranslatorTest {
    private static final Map<String, String> INPUT_TO_OUTPUT = ofEntries(
            entry("if (a + b * (k - 5)) > 0 && !k;", "(a + b * (k - 5)) > 0 && !k"),
            entry("if a == \"test\";", "java.util.Objects.equals(a, \"test\")"),
            entry("if arg(a, 3) < 0;", "a[3] < 0"),
            entry("if arg(a, 0, 3) < 0;", "a[0][3] < 0"),
            entry("if a == b == c;", "java.util.Objects.equals(a, b) && java.util.Objects.equals(b, c)"),
            entry("if test();", "test()"),
            entry("if test(arg(a, 0), b == c, c - a * (a + c));", "test(a[0], java.util.Objects.equals(b, c), c - a * (a + c))"),
            entry("if test(a := c);", "test(a = c)"),
            entry("if a.b - a.k != 0;", "!java.util.Objects.equals(a.b - a.k, 0)")
    );


    @Test
    void parseCondition() {
        for (Map.Entry<String, String> entry : INPUT_TO_OUTPUT.entrySet()) {
            assertEquals(entry.getValue(), UCMMetadataConditionTranslator.translateCondition(entry.getKey()).toString());
        }
    }
}