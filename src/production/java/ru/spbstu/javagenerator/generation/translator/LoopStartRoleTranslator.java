package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.generation.util.Utils;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.parser.metadata.UCMMetadataConditionTranslator;

import java.util.Optional;

public class LoopStartRoleTranslator implements RoleTranslator {

    @Override
    public TranslationResult translate(TranslationContext translationContext, Node node) {
        UcmContextHolder.UcmContext context = UcmContextHolder.getUcmContext();
        CodeBlock.Builder blockBuilder = CodeBlock.builder();

        Node condNode = context.getNodeById(node.getSuccNodesIds().get(0));
        blockBuilder.beginControlFlow(
                "while ($L)",
                UCMMetadataConditionTranslator.translateCondition(condNode.getMetadataList().get(0).getValue())
        );
        TranslationResult translationResult =
                translationContext.nodePathTranslator.translate(
                        translationContext,
                        context.getNodeById(node.getSuccNodesIds().get(0))
                );
        blockBuilder.add(translationResult.codeBlock);


        CodeBlock result = blockBuilder.endControlFlow().build();

        Optional<Node> nodeStartingReturningBranch = translationResult.lastNode.getSuccNodesIds()
                .stream()
                .map(context::getNodeById)
                .filter(n -> startsReturningBranch(n, node))
                .findAny();

        if (nodeStartingReturningBranch.isEmpty()) {
            throw new IllegalStateException("Returning branch for node with id {" + node.getId() + "} not found.");
        }

        translationResult.lastNode.getSuccNodesIds().remove(nodeStartingReturningBranch.get().getId());

        return new TranslationResult(result, translationResult.lastNode);
    }

    private boolean startsReturningBranch(Node node, Node loopStartNode) {
        node = Utils.skipIgnoredNodes(node);

        return node.equals(loopStartNode);
    }
}
