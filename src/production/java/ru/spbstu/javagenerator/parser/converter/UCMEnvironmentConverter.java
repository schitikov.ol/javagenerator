package ru.spbstu.javagenerator.parser.converter;

import com.squareup.javapoet.*;
import ru.spbstu.javagenerator.parser.environment.UCMEnvironmentParser.*;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;

public final class UCMEnvironmentConverter {

    private static final Map<String, TypeName> environmentTypeToJavaType =
            ofEntries(
                    entry("bool", TypeName.BOOLEAN),
                    entry("real", TypeName.DOUBLE),
                    entry("int", TypeName.INT),
                    entry("symb", TypeName.CHAR),
                    entry("string", TypeName.get(String.class))
            );

    public static EnvironmentContext convertEnvironment(EnvironmentDescrContext environmentDescr, String packageName) {
        Collection<EnumTypeContext> enumTypes =
                environmentDescr.enumeratedTypesDeclaration().enumeratedType().stream()
                        .map(enumeratedType -> convertEnumeratedType(enumeratedType, packageName))
                        .collect(Collectors.toSet());

        Collection<AgentTypeContext> agentTypes =
                environmentDescr.listOfAgentDescr().agentDescr().stream()
                        .map(agentDescr -> convertAgentDescr(agentDescr, packageName))
                        .collect(Collectors.toSet());

        Collection<FieldContext> envFields =
                environmentDescr.listOfAttrDescr().attrDescr().stream()
                        .map(attrDescr -> convertAttrDescr(attrDescr, packageName))
                        .collect(Collectors.toSet());

        Collection<FieldContext> envAgents =
                environmentDescr.listOfTypedAgentIds().typedAgentId().stream()
                        .flatMap(typedAgentId -> convertTypedAgentId(typedAgentId, packageName).stream())
                        .collect(Collectors.toSet());

        CodeBlock.Builder staticInitializerBuilder = CodeBlock.builder();

        for (AttributeValueContext attribute : environmentDescr.listOfAttributeValues().attributeValue()) {
            staticInitializerBuilder.addStatement(convertAttributeValue(attribute, envFields));
        }

        for (HiddenAgentParametersContext agentParameters : environmentDescr.listOfHiddenAgentParameters().hiddenAgentParameters()) {
            String agentName = agentParameters.agentId().getText();

            FieldContext agentField = envAgents.stream()
                    .filter(a -> a.getFieldName().equals(agentName))
                    .findAny()
                    .orElseThrow(() -> new IllegalStateException("Cannot find agent with name: " + agentName));

            AgentTypeContext agentType = agentTypes.stream()
                    .filter(a -> a.getTypeName().equals(agentField.getTypeName()))
                    .findAny()
                    .orElseThrow(() -> new IllegalStateException("Cannot find agent type with name: " + agentField.getTypeName()));

            for (AttributeValueContext attribute : agentParameters.listOfAttributeValues().attributeValue()) {
                staticInitializerBuilder.addStatement("$L.$L", agentName, convertAttributeValue(attribute, agentType.getFields()));
            }
        }

        return new EnvironmentContext(
                enumTypes,
                agentTypes,
                envAgents,
                envFields,
                staticInitializerBuilder.build()
        );
    }

    private static EnumTypeContext convertEnumeratedType(EnumeratedTypeContext enumeratedType, String packageName) {
        ClassName className = ClassName.get(packageName, enumeratedType.enumeratedTypeName().getText());

        Collection<String> enumConstants =
                enumeratedType.listOfEnumeratedValues().name().stream()
                        .map(NameContext::getText)
                        .collect(Collectors.toSet());

        return new EnumTypeContext(className, enumConstants);
    }

    private static AgentTypeContext convertAgentDescr(AgentDescrContext agentDescr, String packageName) {
        ClassName className = ClassName.get(packageName, agentDescr.agentTypeName().getText());

        List<AttrDescrContext> attrDescrList = agentDescr.listOfTypedHiddenParameters().typedHiddenParameter().stream().map(TypedHiddenParameterContext::attrDescr).collect(Collectors.toList());

        Collection<FieldContext> fields = attrDescrList.stream()
                .map(attrDescrContext -> convertAttrDescr(attrDescrContext, packageName))
                .collect(Collectors.toSet());

        return new AgentTypeContext(className, fields);
    }

    private static FieldContext convertAttrDescr(AttrDescrContext attrDescr, String packageName) {
        String attrName = attrDescr.attrName().getText();

        if (attrDescr.ARRAY() != null) {
            String elTypeNameStr = attrDescr.typeName().getText();

            TypeName elTypeName = environmentTypeToJavaType.getOrDefault(elTypeNameStr, ClassName.get(packageName, elTypeNameStr));

            TypeName typeName = ArrayTypeName.of(elTypeName);

            CodeBlock.Builder initializerBuilder = CodeBlock.builder().add("new $T", elTypeName);

            for (ListSizeContext listSize : attrDescr.listSize()) {
                initializerBuilder.add("[$L]", listSize.getText());
            }

            CodeBlock initializer = initializerBuilder.build();

            return new FieldContext(
                    typeName,
                    attrName,
                    initializer
            );
        }

        if (attrDescr.LIST() != null) {
            String elTypeNameStr = attrDescr.typeName().getText();

            TypeName elTypeName = environmentTypeToJavaType.getOrDefault(elTypeNameStr, ClassName.get(packageName, elTypeNameStr));

            TypeName typeName = ParameterizedTypeName.get(ClassName.get(List.class), elTypeName);

            CodeBlock initializer = CodeBlock.builder().add(
                    "new $T<>()",
                    ArrayList.class
            ).build();

            return new FieldContext(
                    typeName,
                    attrName,
                    initializer
            );
        }

        String typeNameStr = attrDescr.typeName().getText();

        TypeName typeName = environmentTypeToJavaType.get(typeNameStr);

        if (typeName == null) typeName = ClassName.get(packageName, typeNameStr);

        return new FieldContext(
                typeName,
                attrName,
                null
        );
    }

    private static Collection<FieldContext> convertTypedAgentId(TypedAgentIdContext typedAgentId, String packageName) {
        ClassName agentClass = ClassName.get(packageName, typedAgentId.agentTypeName().getText());

        Set<FieldContext> agentFields = new HashSet<>();

        for (AgentIdContext agentId : typedAgentId.listOfAgentIds().agentId()) {
            String agentName = agentId.getText();

            agentFields.add(new FieldContext(agentClass, agentName, CodeBlock.of("new $T()", agentClass)));
        }

        return agentFields;
    }

    private static CodeBlock convertAttributeValue(AttributeValueContext attributeValue, Collection<FieldContext> attrs) {
        if (attributeValue.attrName() != null) {
            String attrName = attributeValue.attrName().getText();

            FieldContext fieldContext = attrs.stream()
                    .filter(fd -> fd.getFieldName().equals(attrName))
                    .findAny()
                    .orElseThrow(() -> new IllegalStateException("Cannot find attr with name: " + attrName));

            if (attributeValue.listOfValues() != null) {

                if (fieldContext.getTypeName() instanceof ArrayTypeName) {
                    return CodeBlock.builder().add(
                            "$L = $L",
                            attrName,
                            attributeValue.listOfValues().value().stream()
                                    .map(ValueContext::getText)
                                    .collect(Collectors.joining(", ", "{", "}"))
                    ).build();
                }

                if (fieldContext.getTypeName() instanceof ParameterizedTypeName) {
                    return CodeBlock.builder().add(
                            "$L = $T.of($L)",
                            attrName,
                            List.class,
                            attributeValue.listOfValues().value().stream()
                                    .map(ValueContext::getText)
                                    .collect(Collectors.joining(", "))
                    ).build();
                }
            }
            return CodeBlock.builder().add("$L = $L", attrName, attributeValue.value().getText()).build();
        }

        if (attributeValue.initArg() != null) {
            return CodeBlock.builder().add(
                    "$L = $L",
                    convertInitArg(attributeValue.initArg()),
                    attributeValue.value().getText()
            ).build();
        }

        throw new UnsupportedOperationException("Attribute values of type [" + attributeValue.getText() + "] are not supported yet.");
    }

    private static CodeBlock convertInitArg(InitArgContext initArg) {
        CodeBlock.Builder cb = CodeBlock.builder().add(initArg.attrName().getText());

        for (IndexContext index : initArg.index()) {
            try {
                Integer.parseInt(index.getText());
            } catch (NumberFormatException ex) {
                //todo: implement
                throw new UnsupportedOperationException("Non integer arg indicies are not supported yet.");
            }
            cb.add("[$L]", index.getText());
        }

        return cb.build();
    }

    public static final class EnvironmentContext {
        private final Collection<EnumTypeContext> enumTypes;
        private final Collection<AgentTypeContext> agentTypes;
        private final Collection<FieldContext> agentFields;
        private final Collection<FieldContext> attrFields;
        private final CodeBlock staticInitializer;

        public EnvironmentContext(
                Collection<EnumTypeContext> enumTypes,
                Collection<AgentTypeContext> agentTypes,
                Collection<FieldContext> agentFields,
                Collection<FieldContext> attrFields,
                CodeBlock staticInitializer
        ) {
            this.enumTypes = enumTypes;
            this.agentTypes = agentTypes;
            this.agentFields = agentFields;
            this.attrFields = attrFields;
            this.staticInitializer = staticInitializer;
        }

        public Collection<EnumTypeContext> getEnumTypes() {
            return enumTypes;
        }

        public Collection<AgentTypeContext> getAgentTypes() {
            return agentTypes;
        }

        public Collection<FieldContext> getAgentFields() {
            return agentFields;
        }

        public Collection<FieldContext> getAttrFields() {
            return attrFields;
        }

        public CodeBlock getStaticInitializer() {
            return staticInitializer;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof EnvironmentContext)) return false;
            EnvironmentContext that = (EnvironmentContext) o;
            return Objects.equals(enumTypes, that.enumTypes) &&
                    Objects.equals(agentTypes, that.agentTypes) &&
                    Objects.equals(agentFields, that.agentFields) &&
                    Objects.equals(attrFields, that.attrFields) &&
                    Objects.equals(staticInitializer, that.staticInitializer);
        }

        @Override
        public int hashCode() {
            return Objects.hash(enumTypes, agentTypes, agentFields, attrFields, staticInitializer);
        }

        @Override
        public String toString() {
            return "EnvironmentContext{" +
                    "enumTypes=" + enumTypes +
                    ", agentTypes=" + agentTypes +
                    ", agentFields=" + agentFields +
                    ", attrFields=" + attrFields +
                    ", staticInitializer=" + staticInitializer +
                    '}';
        }
    }

    public static final class FieldContext {
        private final TypeName typeName;
        private final String fieldName;
        private final CodeBlock initializer;

        public FieldContext(TypeName typeName, String fieldName, CodeBlock initializer) {
            this.typeName = typeName;
            this.fieldName = fieldName;
            this.initializer = initializer;
        }

        public TypeName getTypeName() {
            return typeName;
        }

        public String getFieldName() {
            return fieldName;
        }

        public Optional<CodeBlock> getInitializer() {
            return Optional.ofNullable(initializer);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FieldContext that = (FieldContext) o;
            return Objects.equals(typeName, that.typeName) &&
                    Objects.equals(fieldName, that.fieldName) &&
                    Objects.equals(initializer, that.initializer);
        }

        @Override
        public int hashCode() {
            return Objects.hash(typeName, fieldName, initializer);
        }

        @Override
        public String toString() {
            return "FieldContext{" +
                    "typeName=" + typeName +
                    ", fieldName='" + fieldName + '\'' +
                    ", initializer=" + initializer +
                    '}';
        }
    }

    public static class TypeContext {
        private final TypeName typeName;

        public TypeContext(TypeName typeName) {
            this.typeName = typeName;
        }

        public TypeName getTypeName() {
            return typeName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TypeContext)) return false;
            TypeContext that = (TypeContext) o;
            return Objects.equals(typeName, that.typeName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(typeName);
        }

        @Override
        public String toString() {
            return "TypeContext{" +
                    "typeName=" + typeName +
                    '}';
        }
    }

    public static final class AgentTypeContext extends TypeContext {
        private final Collection<FieldContext> fields;

        public AgentTypeContext(TypeName typeName, Collection<FieldContext> fields) {
            super(typeName);
            this.fields = fields;
        }

        public Collection<FieldContext> getFields() {
            return fields;
        }

        @Override
        public String toString() {
            return "AgentTypeContext{" +
                    "typeName=" + getTypeName() +
                    ", fields=" + fields +
                    '}';
        }
    }

    public static final class EnumTypeContext extends TypeContext {
        private final Collection<String> constants;

        public EnumTypeContext(TypeName typeName, Collection<String> constants) {
            super(typeName);
            this.constants = constants;
        }

        public Collection<String> getConstants() {
            return constants;
        }

        @Override
        public String toString() {
            return "EnumTypeContext{" +
                    "typeName=" + getTypeName() +
                    ", constants=" + constants +
                    '}';
        }
    }
}
