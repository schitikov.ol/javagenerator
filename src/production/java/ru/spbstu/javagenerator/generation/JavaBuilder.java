package ru.spbstu.javagenerator.generation;

import com.squareup.javapoet.*;
import ru.spbstu.javagenerator.generation.translator.NodePathTranslator;
import ru.spbstu.javagenerator.model.ucmcomponent.Agent;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.StartPoint;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmDef;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpec;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

//TODO: multiple ins and outs in stubs

public class JavaBuilder {

    private final UcmSpec ucmSpec;
    private final Path projectPath;
    private final String packageName;
    private final AgentClassGenerator agentClassGenerator = new AgentClassGenerator();

    public JavaBuilder(UcmSpec ucmSpec, Path projectPath, String packageName) {
        this.ucmSpec = ucmSpec;
        this.projectPath = projectPath;
        this.packageName = packageName;

        UcmContextHolder.setUcmContext(new UcmContextHolder.UcmContext(ucmSpec));
    }

    public void buildJava() throws IOException {
        UcmDef ucmDef = ucmSpec.getUcmDef();


        List<Agent> agents = ucmDef.getAgents();

        //Generate agent classes
        Set<TypeSpec> agentClasses = agentClassGenerator.generate();
        for (TypeSpec agentClass : agentClasses) {
            JavaFile javaFile = JavaFile.builder(packageName, agentClass)
                    .indent("    ")
                    .build();
            javaFile.writeTo(projectPath);
        }

        //Generate agent's instances in Environment class
        List<FieldSpec> fieldSpecs = new ArrayList<>();
        for (Agent agent : agents) {
            ClassName className = ClassName.get(packageName, agent.getClassName());

            FieldSpec fieldSpec = FieldSpec.builder(className, agent.getInstanceName())
                    .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                    .initializer("new $T()", className)
                    .build();

            fieldSpecs.add(fieldSpec);
        }

        //Generate Environment class
        TypeSpec.Builder typeSpecBuilder = TypeSpec.classBuilder("Environment")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addFields(fieldSpecs);

        //Generate main method and stubs
        Optional<Node> startNode = ucmDef.getUcmMaps().get(0).getNodes().stream()
                .filter(node -> node instanceof StartPoint)
                .findFirst();

        CodeBlock translationResult;
        if (startNode.isPresent()) {
            NodePathTranslator translator = new NodePathTranslator((StartPoint) startNode.get(), typeSpecBuilder);
            translationResult = translator.translatePath();
        } else {
            throw new IllegalStateException("StartPoint not found in the main diagram.");
        }

        MethodSpec mainMethod = MethodSpec.methodBuilder("main")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(TypeName.VOID)
                .addParameter(String[].class, "args")
                .addCode(Objects.requireNonNull(translationResult))
                .build();

        TypeSpec typeSpec = typeSpecBuilder.addMethod(mainMethod).build();


        //Write code to file
        JavaFile javaFile =
                JavaFile.builder(packageName, typeSpec)
                        .indent("    ")
                        .build();
        javaFile.writeTo(projectPath);
    }
}