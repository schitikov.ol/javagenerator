package ru.spbstu.javagenerator.model.factory.ucmcomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.factory.ucmspeccomponent.AbstractUcmSpecComponentFactory;
import ru.spbstu.javagenerator.model.ucmcomponent.UcmComponent;

public abstract class AbstractUcmComponentFactory<T extends UcmComponent> extends AbstractUcmSpecComponentFactory<T> {

    @Override
    protected void postCreate(T ucmComponent, Element xmlUcmComponent) throws DataConversionException {
        ucmComponent.setId(xmlUcmComponent.getAttribute("id").getLongValue());
        ucmComponent.setName(xmlUcmComponent.getAttributeValue("name"));
    }

    @Override
    protected abstract T createConcrete();
}
