package ru.spbstu.javagenerator.model.ucmcomponent.node;


public class Responsibility extends Node {
    private long responsibilityDefId;

    public long getResponsibilityDefId() {
        return responsibilityDefId;
    }

    public void setResponsibilityDefId(long responsibilityDefId) {
        this.responsibilityDefId = responsibilityDefId;
    }
}
