grammar UCMEnvironment;

environmentDescr : 'environment' LPAREN
    'types' COLON 'obj' enumeratedTypesDeclaration SEMI
    'attributes' COLON 'obj' listOfAttrDescr SEMI
    'agent_types' COLON 'obj' listOfAgentDescr SEMI
    'agents' COLON 'obj' listOfTypedAgentIds SEMI
    'instances' COLON listOfInstances SEMI
    'axioms' COLON 'obj' LPAREN
    (IDENTIFIER COLON axiom (COMMA IDENTIFIER COLON axiom)* | 'Nil') RPAREN SEMI
    'logic_formula' COLON 'obj' LPAREN (forallVars SEMI)? precondFormula RPAREN SEMI
    'reductions' COLON LPAREN term RPAREN SEMI
    'initial' COLON 'env' LPAREN
        'obj' LPAREN
            'attributes' COLON 'obj' listOfAttributeValues SEMI
            'agent_parameters' COLON 'obj' listOfHiddenAgentParameters
            RPAREN COMMA
        agentComposition
    	RPAREN
RPAREN SEMI;

axiom : LPAREN (forallVars SEMI)? (existVars SEMI)?
    (observableAgentState ARROW)? precondFormula RPAREN;

forallVars : 'Forall' LPAREN varName COLON typeName
    (COMMA varName COLON typeName)* RPAREN;

existVars : 'Exist' LPAREN varName COLON typeName
	(COMMA varName COLON typeName)* RPAREN;

enumeratedTypesDeclaration : LPAREN (enumeratedType (COMMA enumeratedType)* | 'Nil') RPAREN;

enumeratedType : enumeratedTypeName COLON listOfEnumeratedValues;
listOfEnumeratedValues : LPAREN name (COMMA name)* RPAREN;

listOfAttrDescr : LPAREN (attrDescr (COMMA attrDescr)* | 'Nil') RPAREN;

attrDescr : attrName COLON typeName |
    attrName COLON LPAREN listOfEnumeratedTypes RPAREN ARROW typeName |
    attrName COLON ARRAY LPAREN listSize (COMMA listSize)* RPAREN 'of' typeName |
    attrName COLON ARRAY listSize 'of' typeName |
    attrName COLON LIST LPAREN listSize RPAREN 'of' typeName |
    attrName COLON LIST listSize 'of' typeName;

listOfEnumeratedTypes : typeName (COMMA typeName)*;

listOfAgentDescr : LPAREN (agentDescr (COMMA agentDescr)*) RPAREN;

agentDescr : agentTypeName COLON 'obj' listOfTypedHiddenParameters;

listOfTypedHiddenParameters : LPAREN (typedHiddenParameter (COMMA typedHiddenParameter)* | 'Nil') RPAREN;

typedHiddenParameter : attrDescr;

listOfTypedAgentIds : LPAREN (typedAgentId (COMMA typedAgentId)*) RPAREN;

typedAgentId : agentTypeName COLON 'obj' listOfAgentIds;

listOfAgentIds : LPAREN (agentId (COMMA agentId)* | 'Nil') RPAREN;

listOfInstances : LPAREN (instanceName (COMMA instanceName)* | 'Nil') RPAREN;

listOfAttributeValues : LPAREN (attributeValue (COMMA attributeValue)* | 'Nil') RPAREN;

attributeValue : attrName COLON value |
    attrName LPAREN actualParameterList RPAREN COLON value |
    initArg COLON value |
    attrName COLON listOfValues;

listOfValues : LPAREN (value (COMMA value)* | 'Nil') RPAREN;

initArg : 'arg' LPAREN attrName (COMMA index)* RPAREN;

actualParameterList : name (COMMA name)*;

listOfHiddenAgentParameters : LPAREN (hiddenAgentParameters (COMMA hiddenAgentParameters)* | 'Nil') RPAREN;

hiddenAgentParameters : agentId COLON 'obj' listOfAttributeValues;

agentComposition : 'state' LPAREN (observableAgentState (COMMA observableAgentState)* | 'Nil') RPAREN;

observableAgentState : agentTypeName LPAREN agentId COMMA IDENTIFIER RPAREN;

localVar : name;

quantFormula : NOT LPAREN quantFormula RPAREN | firstExpr | quantFormula lMark quantFormula;

firstExpr : expr mark expr | 'empty' LPAREN listName RPAREN | 'is_in_list' LPAREN envElement | value COMMA listName RPAREN | booleanExpression;

booleanExpression : NOT LPAREN booleanExpression RPAREN | booleanExpression mark booleanExpression | expr mark expr | booleanExpression EQU booleanExpression | booleanElem;

booleanElem : POSITIVE_INT | booleanAttribute;

booleanAttribute : envElement;

expr : term (bMark term)*;

term : numericElem | name | booleanElem | LPAREN expr RPAREN | 'Nil';

numericElem : envElement | preReduction | (MINUS? (POSITIVE_REAL | POSITIVE_INT))  | 'abs' LPAREN numericElem RPAREN | 'sizeof' LPAREN listName RPAREN;

envElement : agentAttribute | envAttribute;

agentAttribute : agentAttr | agentArg | agentParamAttr;

agentArg : 'arg' LPAREN agentAttr (COMMA index)+ RPAREN;

index : expr;

agentParamAttr : agentAttr LPAREN paramElem (COMMA paramElem)* RPAREN;

preReduction : preReductionName LPAREN listName RPAREN;

preReductionName : 'get_from_head' | 'get_from_tail';

envAttribute : attrName | envArg | envParamAttr;

envArg : 'arg' LPAREN attrName (COMMA index)+ RPAREN;

envParamAttr : attrName LPAREN paramElem (COMMA paramElem)* RPAREN;

precondFormula : (quantFormula | forall) (lMark (quantFormula | forall))* | 'Nil';

forall : 'Forall' LPAREN localVar COMMA listName COMMA quantFormula RPAREN;

name : IDENTIFIER;
agentId : name;
agentTypeName : name;
varName : name;
attrName : name;
listName : agentAttr | attrName;
agentAttr : agentType WS agentName DOT attrName;
agentType : name;
agentName : name;
enumeratedTypeName : name;
instanceName : name;
value : POSITIVE_INT | POSITIVE_REAL | IDENTIFIER;
typeName : 'bool' | 'real' | 'integer' | 'symb' | name;
lMark : CONJUNCTION | DISJUNCTION;
bMark : PLUS | MINUS | MUL | DIV | MOD;
mark : EQU | BIGGER | SMALLER | BIGGEREQ | SMALLEREQ;
listSize : POSITIVE_INT;
paramElem : name | (name WS name DOT name) | numericElem;

ASSIGN: ':=';
DISJUNCTION: '||';
CONJUNCTION: '&';
SEMI : ';';
COLON : ':';
COMMA : ',';
ARRAY : 'array';
LIST : 'list';
QUOTE : '\'';
DOUBLE_QUOTE : '"';
LPAREN: '(' ;
RPAREN: ')' ;
PLUS : '+' ;
MINUS: '-' ;
DOT : '.' ;
DIV: '/' ;
MOD : 'mod' ;
MUL: '*' ;
NOT : '~' ;
UNDERLINE : '_' ;
BIGGER: '>';
SMALLER: '<';
EQU: '=';
BIGGEREQ: '>=' ;
SMALLEREQ: '<=' ;
ARROW : '->';
NONDET : DISJUNCTION;

fragment DIGIT : [0-9];
fragment ALFA : [a-zA-Z];

IDENTIFIER : (ALFA | UNDERLINE) (ALFA | UNDERLINE | DIGIT)*;

POSITIVE_INT : DIGIT+;
POSITIVE_REAL : POSITIVE_INT (DOT POSITIVE_INT)?;
BOOL : 'true' | 'false';

NCONJ : SEMI;

fragment SPACE : ' ';
fragment NEWLINE : '\r'? '\n';
fragment TAB : '\t';
WS : (SPACE | NEWLINE | TAB)+ -> skip;