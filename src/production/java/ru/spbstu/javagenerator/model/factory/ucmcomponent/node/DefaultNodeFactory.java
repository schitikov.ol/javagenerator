package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

public class DefaultNodeFactory extends AbstractNodeFactory<Node> {
    @Override
    protected Node createConcrete() {
        return new Node();
    }
}
