package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.OrJoin;

public class OrJoinFactory extends AbstractNodeFactory<OrJoin> {
    @Override
    protected OrJoin createConcrete() {
        return new OrJoin();
    }
}
