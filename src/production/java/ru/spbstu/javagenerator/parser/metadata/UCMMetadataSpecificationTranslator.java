package ru.spbstu.javagenerator.parser.metadata;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.parser.converter.UCMMetadataConverter;

public final class UCMMetadataSpecificationTranslator {


    public static CodeBlock translateSpecification(String metadata) {
        UCMMetadataParser parser = UCMMetadataParserFactory.getParserFor(metadata);

        UCMMetadataParser.SpecificationContext specificationContext = parser.specification();

        if (parser.getNumberOfSyntaxErrors() > 0) {
            throw new IllegalStateException("Syntax errors was found while parsing condition.");
        }

        return UCMMetadataConverter.convertSpecification(specificationContext);
    }
}
