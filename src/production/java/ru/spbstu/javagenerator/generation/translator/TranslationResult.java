package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

public class TranslationResult {
    public final CodeBlock codeBlock;
    public final Node lastNode;

    public TranslationResult(CodeBlock codeBlock, Node lastNode) {
        this.codeBlock = codeBlock;
        this.lastNode = lastNode;
    }
}
