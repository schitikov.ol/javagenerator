package ru.spbstu.javagenerator.parser.environment;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public final class UCMEnvironmentParserFactory {
    private UCMEnvironmentParserFactory() {
    }

    public static UCMEnvironmentParser getParserFor(String metadata) {
        return new UCMEnvironmentParser(new CommonTokenStream(new UCMEnvironmentLexer(CharStreams.fromString(metadata))));
    }
}
