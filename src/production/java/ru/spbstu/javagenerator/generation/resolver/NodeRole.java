package ru.spbstu.javagenerator.generation.resolver;

public enum NodeRole {
    PATH_START,
    PATH_END,
    INNER_UCM,
    METHOD,
    CONDITION,
    LOOP_START,
    IF_ELSE_START,
    BRANCHING_END,
    UNRESOLVED,
    IGNORE
}
