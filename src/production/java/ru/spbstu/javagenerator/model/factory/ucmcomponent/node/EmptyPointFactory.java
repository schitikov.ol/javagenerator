package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.EmptyPoint;

public class EmptyPointFactory extends AbstractNodeFactory<EmptyPoint> {
    @Override
    protected EmptyPoint createConcrete() {
        return new EmptyPoint();
    }
}
