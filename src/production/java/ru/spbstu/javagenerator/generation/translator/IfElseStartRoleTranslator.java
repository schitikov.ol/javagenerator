package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.generation.NodeUtils;
import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.generation.resolver.NodeRole;
import ru.spbstu.javagenerator.generation.resolver.NodeRoleResolverManager;
import ru.spbstu.javagenerator.generation.util.Utils;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Responsibility;
import ru.spbstu.javagenerator.parser.metadata.UCMMetadataConditionTranslator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class IfElseStartRoleTranslator implements RoleTranslator {

    @Override
    public TranslationResult translate(TranslationContext translationContext, Node node) {
        NodeRoleResolverManager roleResolverManager = translationContext.nodeRoleResolverManager;
        NodePathTranslator pathTranslator = translationContext.nodePathTranslator;

        CodeBlock.Builder blockBuilder = CodeBlock.builder();

        List<Node> condNodes = node.getSuccNodesIds().stream()
                .map(UcmContextHolder.getUcmContext()::getNodeById)
                .map(Utils::skipIgnoredNodes)
                .collect(Collectors.toList());
        Optional<Node> nodeWithoutCond = condNodes.stream()
                .filter(n -> roleResolverManager.resolve(n) != NodeRole.CONDITION)
                .findFirst();
        List<Node> lastNodes = new ArrayList<>();
        List<Node> endPoints = new ArrayList<>();

        nodeWithoutCond.ifPresent(condNodes::remove);

        addComment(blockBuilder, (Responsibility) condNodes.get(0));
        blockBuilder.beginControlFlow(
                "if ($L)", translateCondition((Responsibility) condNodes.get(0))
        );


        for (int i = 0; i < condNodes.size(); i++) {
            TranslationResult translationResult = pathTranslator.translate(translationContext, condNodes.get(i));
            blockBuilder
                    .add(translationResult.codeBlock);
            if (roleResolverManager.resolve(translationResult.lastNode) == NodeRole.PATH_END) {
                blockBuilder
                        .addStatement("return");
                endPoints.add(translationResult.lastNode);
            } else {
                lastNodes.add(translationResult.lastNode);
            }


            if (i < condNodes.size() - 1) {
                addComment(blockBuilder, (Responsibility) condNodes.get(i + 1));
                blockBuilder.nextControlFlow(
                        "else if ($L)", translateCondition((Responsibility) condNodes.get(i + 1))
                );
            }
        }

        nodeWithoutCond.ifPresent(n -> {
            blockBuilder.nextControlFlow("else");
            TranslationResult translationResult = pathTranslator.translate(translationContext, n);
            blockBuilder
                    .add(translationResult.codeBlock);
            if (roleResolverManager.resolve(translationResult.lastNode) == NodeRole.PATH_END) {
                blockBuilder
                        .addStatement("return");
                endPoints.add(translationResult.lastNode);
            } else {
                lastNodes.add(translationResult.lastNode);
            }
        });

        CodeBlock result = blockBuilder.endControlFlow().build();

        if (lastNodes.isEmpty() && !endPoints.isEmpty()) {
            return new TranslationResult(result, endPoints.get(0));
        }

        if (lastNodes.stream().distinct().count() == 1) {
            return new TranslationResult(result, lastNodes.get(0));
        }

        //        while (lastNodes.stream().distinct().count() != 1) {
        //            //TODO: solve the problem
        //        }

        return new TranslationResult(result, lastNodes.get(0));
    }

    private CodeBlock translateCondition(Responsibility condition) {
        ResponsibilityDef responsibilityDef = UcmContextHolder
                .getUcmContext()
                .getResponsibilityDefById(condition.getResponsibilityDefId());

        return UCMMetadataConditionTranslator.translateCondition(
                responsibilityDef
                        .getMetadataList()
                        .get(0)
                        .getValue()
        );
    }

    private void addComment(CodeBlock.Builder builder, Responsibility condition) {
        ResponsibilityDef responsibilityDef = UcmContextHolder
                .getUcmContext()
                .getResponsibilityDefById(condition.getResponsibilityDefId());

        NodeUtils.addNodeNameComment(builder, responsibilityDef);
    }
}
