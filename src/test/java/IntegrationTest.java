import org.jdom2.JDOMException;
import org.junit.jupiter.api.Test;
import ru.spbstu.javagenerator.generation.JavaBuilder;
import ru.spbstu.javagenerator.model.ModelBuilder;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpec;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class IntegrationTest {

    private static final Pattern
            CLIENT_CLASS_DEF_PATTERN = Pattern.compile("public class Client"),
            CLIENT_DO_SOME_PATTERN = Pattern.compile("public void doSome\\(\\)"),
            CLIENT_DO_OTHER_PATTERN = Pattern.compile("public void doOther\\(\\)"),
            CLIENT_DO_IN_STUB_PATTERN = Pattern.compile("public void doInStub\\(\\)"),
            CLIENT_FOO_PATTERN = Pattern.compile("public void foo\\(\\)"),
            CLIENT_BAR_PATTERN = Pattern.compile("public void bar\\(\\)"),
            ENVIRONMENT_CLASS_DEF_PATTERN = Pattern.compile("public final class Environment"),
            ENVIRONMENT_CLIENT_FIELD_DEF = Pattern.compile("private static final Client c = new Client\\(\\);"),
            ENVIRONMENT_STUB_METHOD =
                    Pattern.compile(
                            "public static void stub\\(\\) \\{\n" +
                                    "        while \\(\\) \\{\n" +
                                    "            c\\.doInStub\\(\\);\n" +
                                    "        }\n" +
                                    "        //EndPoint306\n" +
                                    "    }"
                    ),
            ENVIRONMENT_MAIN_METHOD =
                    Pattern.compile(
                      "public static void main\\(String\\[] args\\) \\{\n" +
                              "        //cond4\n" +
                              "        if \\(a > b\\) \\{\n" +
                              "            //cond5\n" +
                              "            if \\(Objects\\.equals\\(a \\+ b, 0\\)\\) \\{\n" +
                              "            } else \\{\n" +
                              "                //EndPoint293\n" +
                              "                return;\n" +
                              "            }\n" +
                              "        } else \\{\n" +
                              "            c\\.foo\\(\\);\n" +
                              "            //cond1\n" +
                              "            if \\(Objects\\.equals\\(resp, \"Hello\"\\)\\) \\{\n" +
                              "                //cond2\n" +
                              "            } else if \\(Objects\\.equals\\(resp, \"Hello\"\\)\\) \\{\n" +
                              "            } else \\{\n" +
                              "                while \\(Objects\\.equals\\(a, b\\)\\) \\{\n" +
                              "                    c\\.doSome\\(\\);\n" +
                              "                }\n" +
                              "                //cond3\n" +
                              "                if \\(a \\+ b < 0\\) \\{\n" +
                              "                    stub\\(\\);\n" +
                              "                } else \\{\n" +
                              "                    c\\.doOther\\(\\);\n" +
                              "                }\n" +
                              "            }\n" +
                              "        }\n" +
                              "        c\\.bar\\(\\);\n" +
                              "        //ClientEnd\n" +
                              "    }"
                    );


    @Test
    public void test() throws JDOMException, IOException {
        String pathToUcm = Objects.requireNonNull(IntegrationTest.class.getClassLoader().getResource("test.jucm")).getPath();
        Path projectPath = Paths.get("generated/test");
        String packageName = "ru.spbstu.test";

        File ucmFile = new File(pathToUcm);

        UcmSpec ucmSpec = ModelBuilder.buildModel(ucmFile);
        JavaBuilder javaBuilder = new JavaBuilder(ucmSpec, projectPath, packageName);
        javaBuilder.buildJava();

        Path packageRoot = projectPath.resolve(packageName.replace(".", "/"));

        String clientJava = Files.readString(packageRoot.resolve("Client.java"));
        String environmentJava = Files.readString(packageRoot.resolve("Environment.java"));

        assertTrue(CLIENT_CLASS_DEF_PATTERN.matcher(clientJava).find());
        assertTrue(CLIENT_DO_OTHER_PATTERN.matcher(clientJava).find());
        assertTrue(CLIENT_DO_SOME_PATTERN.matcher(clientJava).find());
        assertTrue(CLIENT_DO_IN_STUB_PATTERN.matcher(clientJava).find());
        assertTrue(CLIENT_FOO_PATTERN.matcher(clientJava).find());
        assertTrue(CLIENT_BAR_PATTERN.matcher(clientJava).find());

        assertTrue(ENVIRONMENT_CLASS_DEF_PATTERN.matcher(environmentJava).find());
        assertTrue(ENVIRONMENT_CLIENT_FIELD_DEF.matcher(environmentJava).find());
        assertTrue(ENVIRONMENT_STUB_METHOD.matcher(environmentJava).find());
        assertTrue(ENVIRONMENT_MAIN_METHOD.matcher(environmentJava).find());
    }
}
