package ru.spbstu.javagenerator.parser.converter;

import org.junit.jupiter.api.Test;
import ru.spbstu.javagenerator.parser.environment.UCMEnvironmentParser;
import ru.spbstu.javagenerator.parser.environment.UCMEnvironmentParserFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UCMEnvironmentConverterTest {
    private final Collection<File> environments =
            Optional.of(UCMEnvironmentConverterTest.class.getClassLoader().getResource("environment").toURI())
                    .map(Paths::get)
                    .map(Path::toFile)
                    .map(File::listFiles)
                    .stream()
                    .flatMap(Arrays::stream)
                    .collect(Collectors.toSet());

    private final Map<String, String> inputToOutput = Map.ofEntries(
            Map.entry("env1", "EnvironmentContext{enumTypes=[EnumTypeContext{typeName=ru.spbstu.test.LOC_PORT, constants=[loc_port]}, EnumTypeContext{typeName=ru.spbstu.test.HELO_RESP_CODE, constants=[helo_resp_250, helo_resp_220, helo_resp_221]}, EnumTypeContext{typeName=ru.spbstu.test.CON_RESP_CODE, constants=[conn_resp_503, conn_resp_250, conn_resp_221, conn_resp_220, conn_resp_354]}, EnumTypeContext{typeName=ru.spbstu.test.LOCAL_HOST, constants=[host]}], agentTypes=[AgentTypeContext{typeName=ru.spbstu.test.Client, fields=[FieldContext{typeName=int, fieldName='recepients', initializer=null}]}, AgentTypeContext{typeName=ru.spbstu.test.Server, fields=[FieldContext{typeName=int, fieldName='port', initializer=null}]}], agentFields=[FieldContext{typeName=ru.spbstu.test.Client, fieldName='c', initializer=new ru.spbstu.test.Client()}, FieldContext{typeName=ru.spbstu.test.Server, fieldName='s', initializer=new ru.spbstu.test.Server()}], attrFields=[FieldContext{typeName=boolean, fieldName='receive_state', initializer=null}, FieldContext{typeName=int, fieldName='current', initializer=null}, FieldContext{typeName=char, fieldName='currents', initializer=null}, FieldContext{typeName=int, fieldName='code', initializer=null}, FieldContext{typeName=char, fieldName='dot_s', initializer=null}, FieldContext{typeName=char, fieldName='email_body', initializer=null}, FieldContext{typeName=char, fieldName='email_subject', initializer=null}], staticInitializer=code = 0;\nreceive_state = true;\nc.recepients = 3;\ns.port = 1234;\n}"),
            Map.entry("env2", "EnvironmentContext{enumTypes=[], agentTypes=[AgentTypeContext{typeName=ru.spbstu.test.Client, fields=[]}], agentFields=[], attrFields=[], staticInitializer=}")
    );


    UCMEnvironmentConverterTest() throws URISyntaxException {
    }

    @Test
    void convertEnvironment() throws IOException {
        for (File file : environments) {
            UCMEnvironmentParser parser = UCMEnvironmentParserFactory.getParserFor(Files.readString(file.toPath()));
            assertEquals(inputToOutput.get(file.getName().split("\\.")[0]), UCMEnvironmentConverter.convertEnvironment(parser.environmentDescr(), "ru.spbstu.test").toString());
        }
    }
}