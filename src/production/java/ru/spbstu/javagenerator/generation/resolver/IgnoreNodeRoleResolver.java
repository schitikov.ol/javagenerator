package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.model.ucmcomponent.node.DirectionArrow;
import ru.spbstu.javagenerator.model.ucmcomponent.node.EmptyPoint;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

import java.util.HashSet;
import java.util.Set;

public class IgnoreNodeRoleResolver implements NodeRoleResolver {

    private final Set<Class<? extends Node>> ignoredNodes = new HashSet<>();

    public IgnoreNodeRoleResolver() {
        ignoredNodes.add(EmptyPoint.class);
        ignoredNodes.add(DirectionArrow.class);
    }

    @Override
    public NodeRole resolve(Node node) {
        return NodeRole.IGNORE;
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return ignoredNodes.contains(clazz);
    }
}
