package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import org.jdom2.Attribute;
import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.factory.ucmcomponent.AbstractUcmComponentFactory;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractNodeFactory<T extends Node> extends AbstractUcmComponentFactory<T> {
    private static final Pattern siblingNodeStringPattern = Pattern.compile("//@urndef/@specDiagrams\\." +
            "(?<specDiagramIndex>\\d*)/@connections\\.(?<connectionIndex>\\d*)");

    @Override
    protected void postCreate(T node, Element xmlNode) throws DataConversionException {
        super.postCreate(node, xmlNode);

        Element xmlUcmDef = xmlNode.getParent().getDocument().getRootElement().getChild("urndef");

        Attribute contRefAttribute = xmlNode.getAttribute("contRef");
        if (contRefAttribute != null) {
            node.setContRefId(contRefAttribute.getLongValue());
        }

        String succNodeRefsAttribute = xmlNode.getAttributeValue("succ");
        node.setSuccNodesIds(getSiblingNodesIds(succNodeRefsAttribute, xmlUcmDef, "target"));

        String predNodeRefsAttribute = xmlNode.getAttributeValue("pred");
        node.setPredNodesIds(getSiblingNodesIds(predNodeRefsAttribute, xmlUcmDef, "source"));
    }

    //Orientation can be only "target" or "source"
    private List<Long> getSiblingNodesIds(String siblingNodeRefsAttribute, Element xmlUcmDef, String orientation) throws DataConversionException {
        if (siblingNodeRefsAttribute != null) {
            List<String> siblingNodeRefs = Arrays.asList(siblingNodeRefsAttribute.split(" "));
            int[][] siblingNodesIndicies = new int[siblingNodeRefs.size()][2];

            for (int i = 0; i < siblingNodeRefs.size(); i++) {
                Matcher matcher = siblingNodeStringPattern.matcher(siblingNodeRefs.get(i));

                //noinspection ResultOfMethodCallIgnored
                matcher.matches();

                siblingNodesIndicies[i][0] = Integer.parseInt(matcher.group("specDiagramIndex"));
                siblingNodesIndicies[i][1] = Integer.parseInt(matcher.group("connectionIndex"));
            }

            List<Long> siblingNodeIds = new ArrayList<>();
            for (int[] siblingNodeIndicies : siblingNodesIndicies) {
                Element xmlConnection = xmlUcmDef.getChildren("specDiagrams").get(siblingNodeIndicies[0])
                        .getChildren("connections").get(siblingNodeIndicies[1]);
                siblingNodeIds.add(xmlConnection.getAttribute(orientation).getLongValue());
            }

            return siblingNodeIds;
        } else {
            return null;
        }
    }

    @Override
    protected abstract T createConcrete();
}
