package ru.spbstu.javagenerator.model.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.UcmComponent;

import java.util.List;

public class Node extends UcmComponent {
    private List<Long> succNodesIds;
    private List<Long> predNodesIds;
    private Long contRefId;


    public List<Long> getSuccNodesIds() {
        return succNodesIds;
    }

    public void setSuccNodesIds(List<Long> succNodesIds) {
        this.succNodesIds = succNodesIds;
    }

    public List<Long> getPredNodesIds() {
        return predNodesIds;
    }

    public void setPredNodesIds(List<Long> predNodesIds) {
        this.predNodesIds = predNodesIds;
    }

    public Long getContRefId() {
        return contRefId;
    }

    public void setContRefId(Long contRefId) {
        this.contRefId = contRefId;
    }
}
