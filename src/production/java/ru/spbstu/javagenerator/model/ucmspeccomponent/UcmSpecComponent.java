package ru.spbstu.javagenerator.model.ucmspeccomponent;

import java.util.List;

public abstract class UcmSpecComponent {
    private List<Metadata> metadataList;

    public List<Metadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(List<Metadata> metadataList) {
        this.metadataList = metadataList;
    }
}
