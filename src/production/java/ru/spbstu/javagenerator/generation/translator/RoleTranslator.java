package ru.spbstu.javagenerator.generation.translator;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

public interface RoleTranslator {
    TranslationResult translate(TranslationContext translationContext, Node node);
}
