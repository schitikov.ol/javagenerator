package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Responsibility;

public class ResponsibilityRoleResolver implements NodeRoleResolver {

    @Override
    public NodeRole resolve(Node node) {
        ResponsibilityDef responsibilityDef = UcmContextHolder
                .getUcmContext()
                .getResponsibilityDefById(((Responsibility) node).getResponsibilityDefId());
        if (responsibilityDef.getMetadataList().stream().anyMatch(m -> m.getName().equals("cond"))) {
            return NodeRole.CONDITION;
        } else {
            return NodeRole.METHOD;
        }
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return clazz == Responsibility.class;
    }
}
