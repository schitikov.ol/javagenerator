package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

public interface NodeRoleResolver {
    NodeRole resolve(Node node);

    boolean supports(Class<? extends Node> clazz);
}
