package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Stub;

public class StubRoleResolver implements NodeRoleResolver {

    @Override
    public NodeRole resolve(Node node) {
        return NodeRole.INNER_UCM;
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return clazz == Stub.class;
    }
}
