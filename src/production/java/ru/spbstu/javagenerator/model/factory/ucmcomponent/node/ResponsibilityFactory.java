package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Responsibility;

public class ResponsibilityFactory extends AbstractNodeFactory<Responsibility> {
    @Override
    protected void postCreate(Responsibility responsibility, Element xmlResponsibility) throws DataConversionException {
        super.postCreate(responsibility, xmlResponsibility);

        responsibility.setResponsibilityDefId(xmlResponsibility.getAttribute("respDef")
                .getLongValue());

    }

    @Override
    protected Responsibility createConcrete() {
        return new Responsibility();
    }
}
