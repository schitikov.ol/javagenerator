grammar UCMMetadata;

specification: entity;
entity: comment? instanceDef? localDecl* behavior?;
instanceDef: 'instance' name ';' comment?;
behavior: inState? stimulus? action comment?;
stimulus: (preCondition trigger?) | (trigger preCondition?);
preCondition: 'if' condition inState?;
condition: expr;
inState: 'for' 'state' state;
state: name;
trigger: ('in' envent 'from' instance) | 'timeout' name;
envent: call;
instance: 'lost' | 'found' | name;
action: ('do' '{' stmt* '}' postCondition?) | ';';
postCondition: 'then' assertion nextState?;
nextState: 'nextstate' state ';';
assertion: assigment*;
assigment: lvalue ASSIGN expr ';';
stmt: assigment
    | (expr ';')
    | ';'
    | ('out' (output (',' output)*)? ';')
    | ('reset' (rezet (',' rezet)*)? ';')
    | ('set' (set (',' set)*)? ';')
    | ('stop' ';');
set: type (actual (',' actual)*)? defaultt?;
rezet: type (actual (',' actual)*)?;
output: call 'to' instance;
comment: 'comment' charstring ';';
localDecl: name ':' type defaultt? ';' comment?;
type: name;
defaultt: ASSIGN expr;
expr: expr1 (exprOp expr1)*;
exprOp: DISJUNCTION;
expr1: expr2 (expr1Op expr2)*;
expr1Op: CONJUNCTION;
expr2: expr3 (expr2Op expr3)*;
expr2Op: EQU | NOT_EQU;
expr3: expr4 (expr3Op expr4)*;
expr3Op: SMALLER | BIGGER | SMALLEREQ | BIGGEREQ;
expr4: expr5 (expr4Op expr5)*;
expr4Op: PLUS | MINUS;
expr5: term  (expr5Op term)*;
expr5Op: MUL | DIV | MOD | REM;
term: (termOp term) | primary | imperative;
termOp: MINUS|NOT;
primary: callOrTarget;
callOrTarget: call | ownedAttr | target;
ownedAttr: name '.' name;
target: 'this' | name | literal | 'signal' | bracketExpr;
bracketExpr: '(' expr ')';
literal: bool | charstring | hexstring | bitstring | integer | real;
imperative: 'now'
	| ('timeractive' type ('(' (actual (',' actual)*)? ')')?)
	| 'currentstate'
	| ('create' type '(' state ')');
call: simpleName '(' (actual (',' actual)*)? ')'
    | target;
lvalue: (simpleName '(' (actual (',' actual)*)? ')')
	| 'this'
   	| name
   	| 'signal'
	| ownedAttr
	| ('(' lvalue ')');
actual: (simpleName ASSIGN expr) | expr;
bool: ('true' | 'false');
charstring: STRING;
hexstring: HEXSTRING;
bitstring: BITSTRING;
integer: INTEGER;
real: LITERAL_REAL;
name: ('::')? simpleName ('::' simpleName)*;
simpleName: ID | QOTEDNAME;

ASSIGN: ':=';
DISJUNCTION: '||';
CONJUNCTION: '&&';
PLUS : '+' ;
MINUS: '-' ;
DIV: '/' ;
MOD : 'mod' ;
REM : 'rem';
MUL: '*' ;
NOT : '!' ;
BIGGER: '>';
SMALLER: '<';
EQU: '==';
NOT_EQU: '!=';
BIGGEREQ: '>=' ;
SMALLEREQ: '<=' ;
BITSTRING: '\'' ('0' | '1')* '\'' ('B' | 'b');
HEXSTRING: '\'' ('0'..'9' | 'a'..'f' | 'A'..'F')* '\'' ('H' | 'h');
INTEGER: '0'..'9'+;
EXT_INT: INTEGER ('E' | 'e') ('+' | '-')? INTEGER;
LITERAL_REAL: INTEGER? '.' (EXT_INT | INTEGER) | EXT_INT;
STRING: '"' ('\\' ('b' | 't' | 'n' | 'f' | 'r' | '"' | '\'' | '\\') | ~('\\' | '"'))* '"';
ID: ('_'* (('a'..'z' | 'A'..'Z') | '_' '0'..'9') ('_' | ('a'..'z' | 'A'..'Z') | '0'..'9')*);
QOTEDNAME: '\'' (NAME_ESC | ~('\\' | '\''))+ '\'';
NAME_ESC: '\\' ('b' | 't' | 'n' | 'f' | 'r' | '\\' | '\'');
SL_COMMENT: '//' ~('\n' | '\r')* ('\r'? '\n')?;
WS: (' '|'\t'|'\r'|'\n') -> skip;