package ru.spbstu.javagenerator.model.ucmcomponent;

import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;

import java.util.List;
import java.util.Set;

public class UcmMap extends UcmComponent {
    private List<Node> nodes;
    private Set<Long> contRefsIds;

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public Set<Long> getContRefsIds() {
        return contRefsIds;
    }

    public void setContRefsIds(Set<Long> contRefsIds) {
        this.contRefsIds = contRefsIds;
    }
}
