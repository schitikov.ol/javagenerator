package ru.spbstu.javagenerator.generation;

import ru.spbstu.javagenerator.model.ucmcomponent.Agent;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;
import ru.spbstu.javagenerator.model.ucmcomponent.UcmMap;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmDef;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpec;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class UcmContextHolder {
    private static UcmContext ucmContext;

    public static class UcmContext {
        private final Map<Long, Node> nodesByIdMap;
        private final Map<Long, Agent> agentsByContRefIdMap;
        private final Map<Long, ResponsibilityDef> responsibilityDefByIdMap;
        private final UcmSpec ucmSpec;

        public UcmContext(UcmSpec ucmSpec) {

            this.ucmSpec = ucmSpec;

            UcmDef ucmDef = ucmSpec.getUcmDef();

            nodesByIdMap = ucmDef.getUcmMaps().stream()
                    .flatMap(ucmMap -> ucmMap.getNodes().stream())
                    .collect(Collectors.toMap(Node::getId, node -> node));

            agentsByContRefIdMap = ucmDef.getAgents().stream()
                    .collect(HashMap::new, (map, agent) -> {
                        for (long contRefId : agent.getContRefsIds()) {
                            map.put(contRefId, agent);
                        }
                    }, HashMap::putAll);

            responsibilityDefByIdMap = ucmDef.getResponsibilityDefs().stream()
                    .collect(Collectors.toMap(ResponsibilityDef::getId, responsibilityDef -> responsibilityDef));
        }

        public Node getNodeById(long id) {
            return nodesByIdMap.get(id);
        }

        public Agent getAgentByContRefId(long contRefId) {
            return agentsByContRefIdMap.get(contRefId);
        }

        public ResponsibilityDef getResponsibilityDefById(long id) {
            return responsibilityDefByIdMap.get(id);
        }

        List<UcmMap> getUcmMaps() {
            return ucmSpec.getUcmDef().getUcmMaps();
        }

        List<Agent> getAgents() {
            return ucmSpec.getUcmDef().getAgents();
        }
    }

    public static void setUcmContext(UcmContext ucmContext) {
        UcmContextHolder.ucmContext = ucmContext;
    }

    public static UcmContext getUcmContext() {
        return ucmContext;
    }
}
