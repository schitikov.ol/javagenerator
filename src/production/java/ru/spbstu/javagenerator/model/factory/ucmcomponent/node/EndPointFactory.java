package ru.spbstu.javagenerator.model.factory.ucmcomponent.node;

import ru.spbstu.javagenerator.model.ucmcomponent.node.EndPoint;

public class EndPointFactory extends AbstractNodeFactory<EndPoint> {

    @Override
    protected EndPoint createConcrete() {
        return new EndPoint();
    }
}
