package ru.spbstu.javagenerator;

import org.jdom2.JDOMException;
import ru.spbstu.javagenerator.generation.JavaBuilder;
import ru.spbstu.javagenerator.model.ModelBuilder;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpec;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public final class Launch {
    public static void main(String[] args) throws JDOMException, IOException {
        if (args.length != 3) {
            throw new IllegalArgumentException("Required 3 parameters, found: " + args.length);
        }

        String pathToUcm = args[0];
        String targetDir = args[1];
        String targetPackage = args[2];

        File ucmFile = new File(pathToUcm);

        UcmSpec ucmSpec = ModelBuilder.buildModel(ucmFile);

        JavaBuilder javaBuilder = new JavaBuilder(ucmSpec, Paths.get(targetDir), targetPackage);
        javaBuilder.buildJava();
    }
}
