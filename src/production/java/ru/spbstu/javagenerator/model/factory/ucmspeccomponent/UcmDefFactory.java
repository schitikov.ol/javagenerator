package ru.spbstu.javagenerator.model.factory.ucmspeccomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.factory.ucmcomponent.AbstractUcmComponentFactory;
import ru.spbstu.javagenerator.model.factory.ucmcomponent.AgentFactory;
import ru.spbstu.javagenerator.model.factory.ucmcomponent.ResponsibilityDefFactory;
import ru.spbstu.javagenerator.model.factory.ucmcomponent.UcmMapFactory;
import ru.spbstu.javagenerator.model.ucmcomponent.Agent;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;
import ru.spbstu.javagenerator.model.ucmcomponent.UcmMap;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmDef;

import java.util.ArrayList;
import java.util.List;

public class UcmDefFactory extends AbstractUcmSpecComponentFactory<UcmDef> {

    private static AbstractUcmComponentFactory<Agent> agentFactory = new AgentFactory();
    private static AbstractUcmComponentFactory<ResponsibilityDef> responsibilityDefFactory =
            new ResponsibilityDefFactory();
    private static AbstractUcmComponentFactory<UcmMap> ucmMapFactory =
            new UcmMapFactory();

    @Override
    protected void postCreate(UcmDef ucmDef, Element xmlUcmDef) throws DataConversionException {
        List<Element> xmlAgents = xmlUcmDef.getChildren("components");
        List<Agent> agents = new ArrayList<>();

        for (Element xmlAgent : xmlAgents) {
            String contRefsAttribute = xmlAgent.getAttributeValue("contRefs");
            if (contRefsAttribute == null)
                continue;

            Agent agent = agentFactory.create(xmlAgent);
            agents.add(agent);
        }

        ucmDef.setAgents(agents);

        List<Element> xmlResponsibilityDefs = xmlUcmDef.getChildren("responsibilities");
        List<ResponsibilityDef> responsibilityDefs = new ArrayList<>();

        for (Element xmlResponsibilityDef : xmlResponsibilityDefs) {
            if (xmlResponsibilityDef.getAttribute("respRefs") == null) {
                continue;
            }
            ResponsibilityDef responsibilityDef = responsibilityDefFactory.create(xmlResponsibilityDef);
            responsibilityDefs.add(responsibilityDef);
        }

        ucmDef.setResponsibilityDefs(responsibilityDefs);

        List<Element> xmlUcmMaps = xmlUcmDef.getChildren("specDiagrams");
        List<UcmMap> ucmMaps = new ArrayList<>();

        for (Element xmlUcmMap : xmlUcmMaps) {
            ucmMaps.add(ucmMapFactory.create(xmlUcmMap));
        }

        ucmDef.setUcmMaps(ucmMaps);
    }

    @Override
    protected UcmDef createConcrete() {
        return new UcmDef();
    }
}
