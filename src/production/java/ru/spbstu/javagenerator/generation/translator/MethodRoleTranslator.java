package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.CodeBlock;
import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.model.ucmcomponent.Agent;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Responsibility;

import static ru.spbstu.javagenerator.generation.NodeUtils.abbreviateRespDefName;

public class MethodRoleTranslator implements RoleTranslator {

    @Override
    public TranslationResult translate(TranslationContext translationContext, Node node) {
        Agent parentAgent = UcmContextHolder.getUcmContext().getAgentByContRefId(node.getContRefId());


        CodeBlock result = CodeBlock.builder()
                .addStatement("$N.$N()", parentAgent.getInstanceName(),
                        abbreviateRespDefName(
                                UcmContextHolder.getUcmContext().getResponsibilityDefById(((Responsibility) node).getResponsibilityDefId()).getName()
                        )
                ).build();

        return new TranslationResult(result, node);
    }
}
