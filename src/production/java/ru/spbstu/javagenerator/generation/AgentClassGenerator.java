package ru.spbstu.javagenerator.generation;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import ru.spbstu.javagenerator.generation.resolver.NodeRole;
import ru.spbstu.javagenerator.generation.resolver.NodeRoleResolverManager;
import ru.spbstu.javagenerator.model.ucmcomponent.Agent;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Responsibility;
import ru.spbstu.javagenerator.model.ucmspeccomponent.Metadata;
import ru.spbstu.javagenerator.parser.metadata.UCMMetadataSpecificationTranslator;

import javax.lang.model.element.Modifier;
import java.util.*;
import java.util.stream.Collectors;

import static ru.spbstu.javagenerator.generation.NodeUtils.abbreviateRespDefName;

class AgentClassGenerator {

    private final NodeRoleResolverManager roleResolver = new NodeRoleResolverManager();

    Set<TypeSpec> generate() {
        UcmContextHolder.UcmContext context = UcmContextHolder.getUcmContext();

        List<Agent> agents = context.getAgents();

        HashMap<String, AgentAggregate> agentAggregates = new HashMap<>();
        for (Agent agent : agents) {

            AgentAggregate aggregate = agentAggregates.get(agent.getClassName());

            if (aggregate == null) {
                aggregate = new AgentAggregate();
                agentAggregates.put(agent.getClassName(), aggregate);
            }

            Set<ResponsibilityDef> responsibilityDefs = context.getUcmMaps().stream()
                    .flatMap(ucmMap -> ucmMap.getNodes().stream())
                    .filter(node -> {
                                if (node.getContRefId() == null) {
                                    return false;
                                }
                                return context.getAgentByContRefId(node.getContRefId()).equals(agent);
                            }
                    )
                    .filter(node -> roleResolver.resolve(node) == NodeRole.METHOD)
                    .map(node -> (Responsibility) node)
                    .map(responsibility -> context.getResponsibilityDefById(responsibility.getResponsibilityDefId()))
                    .collect(Collectors.toSet());

            aggregate.responsibilityDefs.addAll(responsibilityDefs);
        }

        Set<TypeSpec> agentClasses = new HashSet<>();

        for (Map.Entry<String, AgentAggregate> entry : agentAggregates.entrySet()) {
            List<MethodSpec> methodSpecs = new ArrayList<>();
            for (ResponsibilityDef respDef : entry.getValue().responsibilityDefs) {
                MethodSpec methodSpec = MethodSpec.methodBuilder(
                        abbreviateRespDefName(respDef.getName())
                )
                        .addModifiers(Modifier.PUBLIC)
                        .addCode(
                                respDef.getMetadataList().stream()
                                        .filter(m -> "code".equals(m.getName()))
                                        .findFirst()
                                        .map(Metadata::getValue)
                                        .map(UCMMetadataSpecificationTranslator::translateSpecification)
                                        .orElse(CodeBlock.builder().build())
                        )
                        .returns(TypeName.VOID)
                        .build();
                methodSpecs.add(methodSpec);
            }

            TypeSpec agentClass = TypeSpec.classBuilder(entry.getKey())
                    .addModifiers(Modifier.PUBLIC)
                    .addMethods(methodSpecs)
                    .build();

            agentClasses.add(agentClass);
        }

        return agentClasses;
    }

    private static class AgentAggregate {
        Set<ResponsibilityDef> responsibilityDefs = new HashSet<>();
    }
}
