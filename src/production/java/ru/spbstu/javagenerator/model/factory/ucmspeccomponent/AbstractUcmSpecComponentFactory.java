package ru.spbstu.javagenerator.model.factory.ucmspeccomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.ucmspeccomponent.Metadata;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpecComponent;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractUcmSpecComponentFactory<T extends UcmSpecComponent> {
    public final T create(Element xmlUcmSpecComponent) throws DataConversionException {
        T ucmSpecComponent = createConcrete();
        List<Element> xmlMetadataList = xmlUcmSpecComponent.getChildren("metadata");

        List<Metadata> metadataList = new ArrayList<>();
        if (!xmlMetadataList.isEmpty()) {
            for (Element xmlMetadata : xmlMetadataList) {
                Metadata metadata = new Metadata();
                metadata.setName(xmlMetadata.getAttributeValue("name"));
                metadata.setValue(xmlMetadata.getAttributeValue("value"));
                metadataList.add(metadata);
            }
        }

        ucmSpecComponent.setMetadataList(metadataList);
        postCreate(ucmSpecComponent, xmlUcmSpecComponent);
        return ucmSpecComponent;
    }

    protected void postCreate(T ucmSpecComponent, Element xmlUcmSpecComponent) throws DataConversionException {
    }

    protected abstract T createConcrete();
}