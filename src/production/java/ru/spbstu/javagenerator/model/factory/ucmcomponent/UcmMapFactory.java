package ru.spbstu.javagenerator.model.factory.ucmcomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import org.jdom2.Namespace;
import ru.spbstu.javagenerator.model.factory.ucmcomponent.node.*;
import ru.spbstu.javagenerator.model.ucmcomponent.UcmMap;
import ru.spbstu.javagenerator.model.ucmcomponent.node.*;

import java.util.*;

public class UcmMapFactory extends AbstractUcmComponentFactory<UcmMap> {


    private static final AbstractUcmComponentFactory<Responsibility> responsibilityFactory = new ResponsibilityFactory();
    private static final AbstractUcmComponentFactory<Stub> stubFactory = new StubFactory();
    private static final AbstractUcmComponentFactory<StartPoint> startPointFactory = new StartPointFactory();
    private static final AbstractUcmComponentFactory<EndPoint> endPointFactory = new EndPointFactory();
    private static final AbstractUcmComponentFactory<DirectionArrow> directionArrowFactory = new DirectionArrowFactory();
    private static final AbstractUcmComponentFactory<OrFork> orForkFactory = new OrForkFactory();
    private static final AbstractUcmComponentFactory<OrJoin> orJoinFactory = new OrJoinFactory();
    private static final AbstractUcmComponentFactory<Node> defaultNodeFactory = new DefaultNodeFactory();
    private static final AbstractUcmComponentFactory<EmptyPoint> emptyPointFactory = new EmptyPointFactory();
    private static final Map<String, AbstractUcmComponentFactory<? extends Node>> nodeTypeToNodeFactoryMap =
            Map.of(
                    "ucm.map:RespRef", responsibilityFactory,
                    "ucm.map:Stub", stubFactory,
                    "ucm.map:StartPoint", startPointFactory,
                    "ucm.map:EndPoint", endPointFactory,
                    "ucm.map:DirectionArrow", directionArrowFactory,
                    "ucm.map:OrFork", orForkFactory,
                    "ucm.map:OrJoin", orJoinFactory,
                    "ucm.map:EmptyPoint", emptyPointFactory
            );

    @Override
    protected void postCreate(UcmMap ucmMap, Element xmlUcmMap) throws DataConversionException {
        super.postCreate(ucmMap, xmlUcmMap);

        //obtain ucm map agents
        //contRefs represents implicit reference to specific agent with it's enclosed node
        Set<Long> contRefsIds = new HashSet<>();
        List<Element> xmlContRefs = xmlUcmMap.getChildren("contRefs");


        for (Element xmlContRef : xmlContRefs) {
            contRefsIds.add(xmlContRef.getAttribute("id").getLongValue());
        }

        ucmMap.setContRefsIds(contRefsIds);

        //obtain ucm map node
        List<Node> ucmMapNodes = new ArrayList<>();
        List<Element> xmlUcmMapNodes = xmlUcmMap.getChildren("nodes");
        for (Element xmlUcmMapNode : xmlUcmMapNodes) {
            String xmlUcmMapNodeType = xmlUcmMapNode.getAttributeValue("type",
                    Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));

            Node ucmMapNode;
            AbstractUcmComponentFactory<? extends Node> nodeFactory = nodeTypeToNodeFactoryMap.get(xmlUcmMapNodeType);

            ucmMapNode = nodeFactory == null ? defaultNodeFactory.create(xmlUcmMapNode) : nodeFactory.create(xmlUcmMapNode);

            ucmMapNodes.add(ucmMapNode);
        }
        ucmMap.setNodes(ucmMapNodes);
    }

    @Override
    protected UcmMap createConcrete() {
        return new UcmMap();
    }
}
