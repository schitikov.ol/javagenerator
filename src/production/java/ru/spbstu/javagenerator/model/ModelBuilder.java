package ru.spbstu.javagenerator.model;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import ru.spbstu.javagenerator.model.factory.ucmspeccomponent.AbstractUcmSpecComponentFactory;
import ru.spbstu.javagenerator.model.factory.ucmspeccomponent.UcmSpecFactory;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpec;

import java.io.File;
import java.io.IOException;

public class ModelBuilder {

    private static final AbstractUcmSpecComponentFactory<UcmSpec> ucmSpecFactory = new UcmSpecFactory();

    public static UcmSpec buildModel(File ucmFile) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(ucmFile);
        Element xmlUcmSpec = document.getRootElement();

        return ucmSpecFactory.create(xmlUcmSpec);
    }
}
