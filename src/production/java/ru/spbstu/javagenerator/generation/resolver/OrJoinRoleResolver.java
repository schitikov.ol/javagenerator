package ru.spbstu.javagenerator.generation.resolver;

import ru.spbstu.javagenerator.generation.UcmContextHolder;
import ru.spbstu.javagenerator.model.ucmcomponent.node.DirectionArrow;
import ru.spbstu.javagenerator.model.ucmcomponent.node.Node;
import ru.spbstu.javagenerator.model.ucmcomponent.node.OrJoin;

public class OrJoinRoleResolver implements NodeRoleResolver {
    @Override
    public NodeRole resolve(Node node) {
        Node succNode = UcmContextHolder.getUcmContext().getNodeById(node.getSuccNodesIds().get(0));

        if (succNode instanceof DirectionArrow) {
            if (succNode.getMetadataList().stream().anyMatch(m -> "cond".equals(m.getName()))) {
                return NodeRole.LOOP_START;
            } else {
                return NodeRole.BRANCHING_END;
            }
        } else {
            return NodeRole.BRANCHING_END;
        }
    }

    @Override
    public boolean supports(Class<? extends Node> clazz) {
        return clazz == OrJoin.class;
    }
}
