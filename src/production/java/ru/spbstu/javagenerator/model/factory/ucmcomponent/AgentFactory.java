package ru.spbstu.javagenerator.model.factory.ucmcomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.ucmcomponent.Agent;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AgentFactory extends AbstractUcmComponentFactory<Agent> {

    @Override
    protected Agent createConcrete() {
        return new Agent();
    }

    @Override
    protected void postCreate(Agent agent, Element xmlAgent) throws DataConversionException {
        super.postCreate(agent, xmlAgent);

        String contRefsAttribute = xmlAgent.getAttributeValue("contRefs");
        String[] strContRefsIds = contRefsAttribute.split(" ");

        Set<Long> contRefsIds = Arrays.stream(strContRefsIds)
                .mapToLong(Long::parseLong)
                .collect(HashSet::new, HashSet::add, HashSet::addAll);

        agent.setContRefsIds(contRefsIds);
    }
}
