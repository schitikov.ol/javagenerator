package ru.spbstu.javagenerator.model.factory.ucmcomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.ucmcomponent.ResponsibilityDef;

public class ResponsibilityDefFactory extends AbstractUcmComponentFactory<ResponsibilityDef> {

    @Override
    protected void postCreate(ResponsibilityDef responsibilityDef, Element xmlResponsibilityDef) throws DataConversionException {
        super.postCreate(responsibilityDef, xmlResponsibilityDef);
        responsibilityDef.setDescription(xmlResponsibilityDef.getAttributeValue("description"));
    }

    @Override
    protected ResponsibilityDef createConcrete() {
        return new ResponsibilityDef();
    }
}
