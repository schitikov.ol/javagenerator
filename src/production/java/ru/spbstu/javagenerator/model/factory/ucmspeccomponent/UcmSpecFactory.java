package ru.spbstu.javagenerator.model.factory.ucmspeccomponent;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmDef;
import ru.spbstu.javagenerator.model.ucmspeccomponent.UcmSpec;

public class UcmSpecFactory extends AbstractUcmSpecComponentFactory<UcmSpec> {

    private static final AbstractUcmSpecComponentFactory<UcmDef> ucmDefFactory = new UcmDefFactory();

    @Override
    protected void postCreate(UcmSpec ucmSpec, Element xmlUcmSpec) throws DataConversionException {
        UcmDef ucmDef = ucmDefFactory.create(xmlUcmSpec.getChild("urndef"));
        ucmSpec.setUcmDef(ucmDef);
    }

    @Override
    protected UcmSpec createConcrete() {
        return new UcmSpec();
    }
}
