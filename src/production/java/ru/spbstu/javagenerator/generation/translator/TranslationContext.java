package ru.spbstu.javagenerator.generation.translator;

import com.squareup.javapoet.TypeSpec;
import ru.spbstu.javagenerator.generation.resolver.NodeRoleResolverManager;
import ru.spbstu.javagenerator.generation.translator.NodePathTranslator;
import ru.spbstu.javagenerator.generation.translator.TranslationState;

public class TranslationContext {
    public final NodePathTranslator nodePathTranslator;
    public final NodeRoleResolverManager nodeRoleResolverManager;
    public final TypeSpec.Builder environmentClassBuilder;
    public TranslationState state = TranslationState.SIMPLE_PATH;

    public TranslationContext(
            NodePathTranslator nodePathTranslator,
            NodeRoleResolverManager nodeRoleResolverManager,
            TypeSpec.Builder environmentClassBuilder
    ) {
        this.nodePathTranslator = nodePathTranslator;
        this.nodeRoleResolverManager = nodeRoleResolverManager;
        this.environmentClassBuilder = environmentClassBuilder;
    }
}
