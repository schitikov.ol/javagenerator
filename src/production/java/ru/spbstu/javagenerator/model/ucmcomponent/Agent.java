package ru.spbstu.javagenerator.model.ucmcomponent;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Agent extends UcmComponent {
    private final Pattern agentNamePattern = Pattern.compile("(?<className>[a-zA-Z_$][a-zA-Z\\d_$]*)#(?<instanceName>[a-zA-Z_$][a-zA-Z\\d_$]*)");
    private Set<Long> contRefsIds;
    private String className;
    private String instanceName;

    public Set<Long> getContRefsIds() {
        return contRefsIds;
    }

    public void setContRefsIds(Set<Long> contRefsIds) {
        this.contRefsIds = contRefsIds;
    }

    public String getClassName() {

        if (className != null) {
            return className;
        }

        Matcher matcher = agentNamePattern.matcher(getName());
        //noinspection ResultOfMethodCallIgnored
        matcher.matches();
        className = matcher.group("className");
        return className;
    }

    public String getInstanceName() {

        if (instanceName != null) {
            return instanceName;
        }

        Matcher matcher = agentNamePattern.matcher(getName());
        //noinspection ResultOfMethodCallIgnored
        matcher.matches();
        instanceName = matcher.group("instanceName");
        return instanceName;
    }
}
